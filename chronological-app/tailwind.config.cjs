/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  daisyui: {
    themes: ['emerald', 'forest'],
  },
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {},
  plugins: [require('daisyui', '@tailwindcss/forms')],
};
