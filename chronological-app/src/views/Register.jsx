import PublicHeader from "../components/Headers/PublicHeader";
import Footer from "../components/Footer/Footer";
import RegisterForm from "../components/RegisterForm/RegisterForm";
import { Helmet } from "react-helmet";

export default function Register() {
  return (
    <div>
      <Helmet title="Register" />
      <PublicHeader />
      <RegisterForm />
      <Footer />
    </div>
  );
}
