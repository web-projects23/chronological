import PublicHeader from "../components/Headers/PublicHeader";
import Footer from "../components/Footer/Footer";
import LoginForm from "../components/LoginForm/LoginForm";
import { Helmet } from "react-helmet";

export default function Login() {
  return (
    <div>
      <Helmet title="Login" />
      <PublicHeader />
      <LoginForm />
      <Footer />
    </div>
  );
}
