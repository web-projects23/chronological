import PublicHeader from "../components/Headers/PublicHeader";
import Footer from "../components/Footer/Footer";
import PublicHero from "../components/PublicHero/PublicHero";
import { Helmet } from "react-helmet";

export default function Home() {
  return (
    <div className="h-screen flex flex-col">
      <Helmet title="Home" />
      <PublicHeader />
      <PublicHero />
      <Footer />
    </div>
  );
}
