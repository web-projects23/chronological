import { AppContext } from "../context/app.context";
import { useContext, useEffect, useState } from "react";
import AddContactList from "../components/ContactLists/AddContactList";
import { Outlet, useNavigate, useLocation, useParams } from "react-router-dom";
import {
  getContactListsByOwner,
  deleteContactList,
} from "../services/contact-list.service";
import { Helmet } from "react-helmet";
import { RiFileEditLine, RiDeleteBinLine } from "react-icons/ri";
import { HiOutlineUserGroup } from "react-icons/hi";

export default function MyContacts({ showModal, toggleModal }) {
  const { userData } = useContext(AppContext);
  const [allContactLists, setAllContactLists] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    getContactListsByOwner(userData?.username)
      .then(setAllContactLists)
      .catch(console.error);
  }, [location, userData]);

  const deleteList = async (listId, username) => {
    try {
      await deleteContactList(listId, username);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <Helmet title="My contacts" />
      <div className="responsive md:ml-64 p-3">
        <label
          className="btn glass text-white text-sm bg-[#6267f1] hover:bg-violet-700 m-3 "
          onClick={() => {
            toggleModal();
            navigate("add-contact-list");
          }}
        >
          Create List
        </label>
        <div className="h-50 w-full rounded-lg bg-white ">
          <div className="flex items-center justify-between border-b-4 border-[#6267f1]">
            <div className="flex items-center p-3 text-gray-700 text-lg font-bold">
              <HiOutlineUserGroup className="w-6 h-6" />
              Your contact lists
            </div>
          </div>

          {!allContactLists || allContactLists.length === 0 ? (
            <div className="md:pl-64 hero-content text-center mt-20">
              <div className="max-w-md">
                <h1 className="text-3xl font-bold">No contact lists yet</h1>
                <p className="py-6">
                  Create your own contact list with friends, family or someone
                  you know.
                </p>
              </div>
            </div>
          ) : (
            allContactLists.map((currList, i) => (
              <div
                className="flex items-center justify-between border-b"
                key={i}
              >
                <div className="p-3 text-md text-gray-600">
                  {currList.name}
                  <p className="text-xs">{`${currList?.contacts && Object.values(currList.contacts).length} Contacts`}</p>
                </div>
                {Object.values(currList.contacts).length > 1 && (
                  <div className="avatar-group -space-x-6">
                    <div className="avatar">
                      <div className="w-12">
                        <img
                          src={Object.values(currList.contacts)[0].avatarUrl} />
                      </div>
                    </div>
                    <div className="avatar">
                      <div className="w-12">
                        <img
                          src={Object.values(currList.contacts)[1].avatarUrl} />
                      </div>
                    </div>
                    {Object.values(currList.contacts).length > 2 && (
                      <div className="avatar placeholder">
                        <div className="w-12 bg-neutral-focus text-neutral-content">
                          <span>
                            +{Object.values(currList.contacts).length - 2}
                          </span>
                        </div>
                      </div>
                    )}
                  </div>
                )}
                <div className="p-3 flex">
                  <button
                    onClick={() => navigate(`${currList.id}`)}
                    className="text-slate-800 hover:text-blue-500 text-sm bg-white hover:bg-slate-100 border border-slate-200 rounded-l-lg font-medium px-2 py-2 inline-flex space-x-1 items-center"
                  >
                    <span>
                      <RiFileEditLine />
                    </span>
                    <span>Edit</span>
                  </button>
                  <button
                    onClick={() => {
                      deleteList(currList.id, userData.username)
                      setAllContactLists(
                        allContactLists.filter(
                          (c) => c.id !== allContactLists.id
                        )
                      );
                      navigate('/my-contacts');
                    }}
                    className="text-slate-800 hover:text-red-500 text-sm bg-white hover:bg-slate-100 border border-slate-200 rounded-r-lg font-medium px-2 py-2 inline-flex space-x-1 items-center"
                  >
                    <span>
                      <RiDeleteBinLine />
                    </span>
                    <span>Delete</span>
                  </button>
                </div>
              </div>
            ))
          )}
        </div>
      </div>

      <input
        type="checkbox"
        className="modal-toggle"
        checked={showModal}
        onChange={toggleModal}
      />
      <AddContactList toggleModal={toggleModal} />
      <Outlet />
    </div>
  );
}
