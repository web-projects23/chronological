import { useContext, useEffect, useState } from "react";
import MyEventsCard from "../components/MyEventsCard/MyEventsCard";
import { AppContext } from "../context/app.context";
import { getAllUniqueEvents } from "../services/events.service";
import { TbCalendarTime } from "react-icons/tb";
import { useLocation, useSearchParams } from "react-router-dom";
import NoEvents from "../components/MyEventsCard/NoEvents";
import { timerSeconds } from "../common/validation-enum";
import SearchBar from "../components/SearchBar/SearchBar";
import { Helmet } from "react-helmet";

export default function MyEvents() {
  const { userData } = useContext(AppContext);
  const [events, setEvents] = useState([]);
  const [search, setSearch] = useSearchParams();
  const [keyword, setKeyword] = useState(search.get("event") || "");
  const location = useLocation();
  const [showLoading, setShowLoading] = useState(true);

  useEffect(() => {
    getAllUniqueEvents(userData?.username)
      .then((events) => {
        setEvents(events);
        setShowLoading(false);
      })
      .catch(console.error);
  }, [location, userData]);

  useEffect(() => {
    const timer = setTimeout(
      () =>
        getAllUniqueEvents(userData?.username)
          .then((allEvents) =>
            allEvents.filter((event) => {
              if (event.title.toLowerCase().includes(keyword.toLowerCase())) {
                return event;
              }
            })
          )
          .then(setEvents)
          .catch(console.error),
      timerSeconds
    );
    return () => clearTimeout(timer);
  }, [keyword, userData]);

  const setSearchParams = (value) => {
    setKeyword(value);
    setSearch({ event: value });
  };

  return (
    <div>
      <Helmet title="My events" />
      <div className="md:pl-64 m-10 flex flex-col justify-center align-center">
        <span className="flex items-center justify-center mr-2 ">
          <TbCalendarTime className="mr-2 text-2xl" />
          <p className="text-2xl"> My events </p>
        </span>
        <SearchBar setSearchParams={setSearchParams} />
        {showLoading ? (
          ""
        ) : events?.length !== 0 ? (
          events.map((event) => (
            <ol key={event.id} className="relative border-l border-gray-200">
              <MyEventsCard event={event} />
            </ol>
          ))
        ) : (
          <NoEvents />
        )}
      </div>
    </div>
  );
}
