// import PublicHeader from '../components/Headers/PublicHeader';
// import Footer from '../components/Footer/Footer';
import Error from "../assets/images/404.jpg";
import { useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet";

export default function NotFound() {
  const navigate = useNavigate();

  return (
    <div className="hero text-black min-h-screen bg-white">
      <Helmet title="Not found" />
      <div className="flex items-center flex-col justify-center lg:flex-row py-28 px-6 md:px-24 md:py-20 lg:py-32 gap-16 lg:gap-28">
        <div className="w-full lg:w-1/4">
          <img src={Error} alt="NotFoundGif" className="h-full w-full" />
        </div>
        <div className="w-full lg:w-1/2">
          <h2 className="py-4 text-3xl lg:text-4xl font-extrabold text-gray-800">
            Where is the content? What is this place?
          </h2>
          <p className="py-4 text-base text-gray-800">
            The content you’re looking for doesn’t exist. Either it was removed,
            or you mistyped the link.
          </p>
          <p className="py-2 text-base text-gray-800">
            Sorry about that! Please visit our homepage to get where you need to
            go.
          </p>
          <button
            onClick={() => navigate("/calendar/month")}
            className="w-full lg:w-auto my-4 border rounded-md px-1 sm:px-16 py-5 bg-indigo-500 text-white hover:bg-indigo-900 focus:outline-none focus:ring-2 focus:ring-indigo-700 focus:ring-opacity-50"
          >
            Go back to Homepage
          </button>
        </div>
      </div>
    </div>
  );
}
