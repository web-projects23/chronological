import UserCard from "../components/UserCard/UserCard";
import { useState, useEffect } from "react";
import { useParams, useSearchParams } from "react-router-dom";
import { allUsers, updateRole } from "../services/users.service";
import { timerSeconds } from "../common/validation-enum";
import { Helmet } from "react-helmet";
import SearchBar from "../components/SearchBar/SearchBar";

export default function Users() {
  const [users, setUsers] = useState([]);
  const [search, setSearch] = useSearchParams();
  const [keyword, setKeyword] = useState(search.get("user") || "");
  const { uid } = useParams();

  useEffect(() => {
    allUsers().then(setUsers).catch(console.error);
  }, []);

  useEffect(() => {
    const timer = setTimeout(
      () =>
        allUsers()
          .then((allUsers) =>
            allUsers.filter((user) => {
              if (user === null || user === undefined) {
                return null;
              } else if (
                user.username.toLowerCase().includes(keyword.toLowerCase()) ||
                user.phoneNumber.includes(keyword) ||
                user.firstName.toLowerCase().includes(keyword.toLowerCase()) ||
                user.lastName.toLowerCase().includes(keyword.toLowerCase())
              ) {
                return user;
              }
            })
          )
          .then(setUsers)
          .catch(console.error),
      timerSeconds
    );
    return () => clearTimeout(timer);
  }, [keyword]);

  const setSearchParams = (value) => {
    setKeyword(value);
    setSearch({ user: value });
  };

  const changeRole = (username, role) => {
    updateRole(username, role).then(() => {
      const idx = users.findIndex((user) => user.username === username);
      users[idx].role = role;
      setUsers([...users]);
    });
  };

  return (
    <div>
      <Helmet title="Users" />
      <div className="md:pl-64">
        <SearchBar setSearchParams={setSearchParams} />
        <ul
          role="list"
          className="m-5 grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3"
        >
          {users
            .sort(function (a, b) {
              return b.registeredOn - a.registeredOn;
            })
            .map((user) => {
              return (
                <UserCard
                  key={user.uid}
                  user={user}
                  uid={uid}
                  changeRole={changeRole}
                />
              );
            })}
        </ul>
      </div>
    </div>
  );
}
