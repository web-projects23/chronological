import UserProfileForm from "../components/UserProfileForm/UserProfileForm";
import { Helmet } from "react-helmet";

export default function UserProfile() {
  return (
    <div>
      <Helmet title="My Profile" />
      <UserProfileForm />
    </div>
  );
}
