import { useContext, useEffect, useState } from "react";
import { Outlet, useNavigate, useLocation, useParams } from "react-router-dom";
import {
  getContactListById,
  deleteContact,
  deleteContactList,
} from "../services/contact-list.service";
import { Helmet } from "react-helmet";
import { RiDeleteBinLine } from "react-icons/ri";
import AddContact from "../components/ContactLists/AddContact";
import useWindowSize from "../hooks/windowSize";
import { AppContext } from "../context/app.context";

export default function SingleContactList({ showModal, toggleModal }) {
  const { userData } = useContext(AppContext);
  const [currList, setCurrList] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();
  const { listId } = useParams();
  const windowSize = useWindowSize();

  useEffect(() => {
    getContactListById(listId)
      .then((data) => setCurrList(data[0]))
      .catch(console.error);
  }, [location]);

  const deleteContactFromList = async (username, listId) => {
    try {
      await deleteContact(username, listId);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <Helmet title="Single Contact List" />
      <div className="md:ml-64">
        <label
          className="btn btn-ghost m-3 bg-[#6267f1] text-white"
          onClick={() => {
            navigate("add-contact");
            toggleModal();
          }}
        >
          Add Contact
        </label>
        <div className="md:px-3 px-2 py-4 w-full">
          <table className="min-w-full bg-white shadow overflow-hidden rounded border-gray-200">
            <thead className="bg-[#6267f1] text-white">
              <tr>
                <th className="text-left p-2 uppercase font-semibold text-md">
                  Avatar
                </th>
                <th className="text-left py-2 pl-5 uppercase font-semibold text-md">
                  Full name
                </th>
                <th className="text-left py-2 pl-10 uppercase font-semibold text-md">
                  Phone
                </th>
                {windowSize.width > 600 && (
                  <th className="text-left py-2 pl-16 uppercase font-semibold text-md">
                    Email
                  </th>
                )}
                <th className="text-left py-2 uppercase font-semibold text-sm"></th>
              </tr>
            </thead>
            {Object.values(currList).map((contact, i) => (
              <tbody className="text-gray-700" key={i}>
                <tr className="border-b-2">
                  <td>
                  <div className="flex avatar ml-2">
                    <div className="w-12 rounded-full">
                      <img src={contact.avatarUrl} alt="avatar" />
                    </div>
                  </div>
                  </td>
                  <td className="text-left py-3 px-4">{`${contact.firstName} ${contact.lastName}`}</td>
                  <td className="text-left py-3 px-4">
                    <a
                      className="hover:text-blue-500"
                      href={`tel: ${contact.phoneNumber}`}
                    >
                      {contact.phoneNumber}
                    </a>
                  </td>
                  {windowSize.width > 600 && (
                    <td className="text-left py-3 px-4">{contact.email}</td>
                  )}
                  <td className="text-left py-3">
                    {
                      <button
                        className="text-slate-800 hover:text-red-500 text-xl bg-white hover:bg-slate-100 border rounded-md border-slate-200 font-medium px-2 py-2 inline-flex space-x-1 items-center"
                        onClick={() =>
                          {
                            if (Object.values(currList).length === 1) {
                              return deleteContactList(listId, userData.username)
                                .then(() => navigate('/my-contacts'));
                            } else {
                              return deleteContactFromList(contact.username, listId);
                            }
                          }
                        }
                      >
                        <span>
                          <RiDeleteBinLine />
                        </span>
                      </button>
                    }
                  </td>
                </tr>
              </tbody>
            ))}
          </table>
        </div>
      </div>
      <input
        type="checkbox"
        className="modal-toggle"
        checked={showModal}
        onChange={toggleModal}
      />
      <AddContact toggleModal={toggleModal} />
      <Outlet />
    </div>
  );
}
