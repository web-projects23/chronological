import Asen from "../assets/images/Asen.png";
import Ivana from "../assets/images/Ivana.png";
import Toni from "../assets/images/Toni.png";
import { SiGitlab } from "react-icons/si";
import { SiLinkedin } from "react-icons/si";
import { Helmet } from "react-helmet";

const team = [
  {
    name: "Asen Georgiev",
    role: "Telerik Academy Student",
    imageUrl: Asen,
    gitlabUrl: "https://gitlab.com/Asen.G",
    linkedinUrl: "https://www.linkedin.com/in/asen-georgiev-63b1b710a/",
  },
  {
    name: "Ivana Georgieva",
    role: "Telerik Academy Student",
    imageUrl: Ivana,
    gitlabUrl: "https://gitlab.com/Ivana1225",
    linkedinUrl: "https://www.linkedin.com/in/ivana-georgieva-a80079187/",
  },
  {
    name: "Toni Velikov",
    role: "Telerik Academy Student",
    imageUrl: Toni,
    gitlabUrl: "https://gitlab.com/Skorpionas99",
    linkedinUrl: "https://www.linkedin.com/in/toni-velikov-931638225/",
  },
];

export default function AboutUs() {
  return (
    <div>
      <Helmet title="About us" />
      <div className="bg-white md:pl-64">
        <div className="mx-auto py-12 px-4 max-w-7xl sm:px-6 lg:px-8 lg:py-24">
          <div className="space-y-12 lg:grid lg:grid-cols-3 lg:gap-8 lg:space-y-0">
            <div className="space-y-5 sm:space-y-4">
              <h2 className="text-3xl font-extrabold tracking-tight sm:text-4xl">
                About Us
              </h2>
              <p className="text-xl text-gray-500">
                Our mission is to help people organize their day-to-day life and
                help them keep track of their upcoming events.
              </p>
            </div>
            <div className="lg:col-span-2">
              <ul
                role="list"
                className="space-y-12 sm:divide-y sm:divide-gray-200 sm:space-y-0 sm:-mt-8 lg:gap-x-8 lg:space-y-0"
              >
                {team.map((person) => (
                  <li key={person.name} className="sm:py-8">
                    <div className="space-y-4 sm:grid sm:grid-cols-3 sm:items-start sm:gap-6 sm:space-y-0">
                      <div className="aspect-w-3 aspect-h-2 sm:aspect-w-3 sm:aspect-h-4">
                        <img
                          className="object-cover shadow-lg rounded-lg"
                          src={person.imageUrl}
                          alt=""
                        />
                      </div>
                      <div className="sm:col-span-2">
                        <div className="space-y-4">
                          <div className="text-lg leading-6 font-medium space-y-1">
                            <h3>{person.name}</h3>
                            <p className="text-gray-400">{person.role}</p>
                          </div>
                          <div className="text-lg">
                            <p className="text-black">{person.bio}</p>
                          </div>
                          <ul role="list" className="flex space-x-5">
                            <li>
                              <a
                                href={person.gitlabUrl}
                                target="_blank"
                                rel="noopener noreferrer"
                                className="text-orange-500 hover:text-orange-700"
                              >
                                <span className="sr-only">Gitlab</span>
                                <SiGitlab />
                              </a>
                            </li>
                            <li>
                              <a
                                href={person.linkedinUrl}
                                target="_blank"
                                rel="noopener noreferrer"
                                className="text-blue-500 hover:text-blue-400"
                              >
                                <span className="sr-only">LinkedIn</span>
                                <SiLinkedin />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
