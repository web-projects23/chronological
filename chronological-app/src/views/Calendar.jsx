import CalendarViewsNav from "../components/Calendar/CalendarViewsNav";
import { Outlet } from "react-router-dom";
import AddEventForm from "../components/AddEventForm/AddEvent";
import { Helmet } from "react-helmet";
import DetailedViewModal from "../components/Calendar/DetailedViewModal";
import { useContext } from "react";
import { AppContext } from "../context/app.context";

export default function Calendar({ showModal, toggleModal }) {
  const { userData } = useContext(AppContext);
  return (
    <div>
      <Helmet title="Calendar" />
      <div>
        <input
          type="checkbox"
          id="my-modal-6"
          className="modal-toggle"
          checked={showModal}
          onChange={toggleModal}
        />
        <AddEventForm toggleModal={toggleModal} />
        <DetailedViewModal />
        <CalendarViewsNav />
        {userData?.username && <Outlet />}
      </div>
    </div>
  );
}
