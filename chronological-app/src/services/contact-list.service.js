import { ref, push, get, update } from "firebase/database";
import { db } from "../config/firebase-config";

export const addContactList = async (name, username, contacts) => {
  const contactList = {
    name,
    owner: username,
    contacts,
    createdOn: Date.now(),
  };

  const { key } = await push(ref(db, "contactLists"), contactList);

  update(ref(db), {
    [`contactLists/${key}/id`]: key,
    [`users/${username}/contactLists/${key}`]: true,
  });

  return key;
};

export const getContactListById = async (listId) => {
  const snapshot = await get(ref(db, `contactLists/${listId}`));

  const contactList = snapshot.val();

  if (contactList !== null) {
    return Object.values(contactList);
  }
};

export const addContact = async (listId, contacts) => {
  return update(ref(db), {
    [`contactLists/${listId}/contacts`]: contacts,
  });
};

export const deleteContact = async (contactUsername, listId) => {
  return update(ref(db), {
    [`contactLists/${listId}/contacts/${contactUsername}`]: null,
  });
};

export const getAllContactLists = async () => {
  const snapshot = await get(ref(db, "contactLists"));

  const allContactLists = snapshot.val();

  if (allContactLists !== null) {
    return Object.keys(allContactLists).map((key) => {
      const contactListInfo = allContactLists[key];

      return contactListInfo;
    });
  }
};

export const deleteContactList = async (listId, username) => {
  return update(ref(db), {
    [`users/${username}/contactLists/${listId}`]: null,
    [`contactLists/${listId}`]: null,
  });
};

export const getContactListsByOwner = async (username) => {
  const snapshot = await get(ref(db, "contactLists"));

  const allContactLists = snapshot.val();

  if (allContactLists !== null) {
    return Object.values(allContactLists).filter(
      (currList) => username === currList.owner
    );
  }
};
