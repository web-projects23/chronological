import {
  ref,
  get,
  set,
  update,
  query,
  equalTo,
  orderByChild,
} from "firebase/database";
import { userRole } from "../common/user-role";
import { db } from "../config/firebase-config";

export const getUser = async (username) => {
  const snapshot = await get(ref(db, `users/${username}`));

  return snapshot.val();
};

export const allUsers = async () => {
  const snapshot = await get(ref(db, "users"));

  const allUsers = snapshot.val();

  if (allUsers !== null) {
    return Object.keys(allUsers).map((key) => {
      const usersData = allUsers[key];
      return {
        ...usersData,
      };
    });
  }
};

export const getUserById = async (uid) => {
  const snapshot = await get(
    query(ref(db, "users"), orderByChild("uid"), equalTo(uid))
  );

  const { ...value } = snapshot.val()?.[Object.keys(snapshot.val())?.[0]];

  return {
    ...value,
  };
};

export const getUserByPhone = async (phoneNumber) => {
  const snapshot = await get(
    query(ref(db, "users"), orderByChild("phoneNumber"), equalTo(phoneNumber))
  );

  return snapshot.exists() ? Object.keys(snapshot.val())[0] : null;
};

export const updateName = async (username, firstName, lastName) => {
  return update(ref(db), {
    [`users/${username}/firstName`]: firstName,
    [`users/${username}/lastName`]: lastName,
  });
};

export const updatePhoneNumber = async (username, phoneNumber) => {
  return update(ref(db), {
    [`users/${username}/phoneNumber`]: phoneNumber,
  });
};

export const createUser = async (
  uid,
  firstName,
  lastName,
  username,
  phoneNumber,
  email,
  avatarUrl = "https://www.citypng.com/public/uploads/preview/png-round-blue-contact-user-profile-icon-11639786938sxvzj5ogua.png",
  role = userRole.BASIC
) => {
  const user = await getUser(username);
  const phoneNum = await getUser(phoneNumber);

  if (user !== null) {
    throw new Error(`User with username ${username} already exists!`);
  }
  if (phoneNum !== null) {
    throw new Error(`Phone number: ${phoneNumber} already exists!`);
  }

  const userData = {
    uid,
    firstName,
    lastName,
    username,
    phoneNumber,
    email,
    avatarUrl,
    registeredOn: Date.now(),
    role,
  };

  await set(ref(db, `users/${username}`), userData);

  return { ...userData };
};

export const updateAvatar = (username, url) => {
  return update(ref(db), { [`users/${username}/avatarUrl`]: url });
};

export const updateRole = (username, role) => {
  return update(ref(db), { [`users/${username}/role`]: role });
};
