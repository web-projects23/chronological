import { addMonths, addWeeks, addYears, format, parseISO } from "date-fns";
import { ref, push, get, update } from "firebase/database";
import { validation } from "../common/validation-enum";
import { db } from "../config/firebase-config";

export const addEvent = async (
  title,
  startDate,
  startTime,
  endDate,
  endTime,
  additionalInfo,
  category,
  repeats,
  privateCheckbox,
  contacts,
  username,
  avatarUrl
) => {
  const event = {
    title,
    startDate,
    startTime,
    endDate,
    endTime,
    additionalInfo,
    category,
    repeats,
    privateCheckbox,
    contacts,
    username,
    avatarUrl,
  };

  const { key } = await push(ref(db, "events"), event);

  update(ref(db), {
    [`users/${username}/events/${key}`]: true,
  });
  return key;
};

export const getAllEvents = async (username) => {
  const snapshot = await get(ref(db, "events"));

  if (!snapshot.exists()) {
    return [];
  }

  return Object.entries(snapshot.val())
    .map((entry) => {
      const [uid, event] = entry;
      return {
        id: uid,
        ...event,
      };
    })
    .filter((event) => event.username === username)
    .flatMap((event) => {
      if (event.repeats === "weekly") {
        return [
          ...Array(
            validation.NUM_YEARS_TO_REPEAT_EVENT *
              validation.NUM_WEEKS_IN_ONE_YEAR
          ).keys(),
        ].map((weeksToAdd) => ({
          ...event,
          startDate: format(
            addWeeks(parseISO(event.startDate), weeksToAdd),
            "yyyy-MM-dd"
          ),
          endDate: format(
            addWeeks(parseISO(event.endDate), weeksToAdd),
            "yyyy-MM-dd"
          ),
        }));
      } else if (event.repeats === "monthly") {
        return [
          ...Array(
            validation.NUM_YEARS_TO_REPEAT_EVENT *
              validation.NUM_MONTHS_IN_ONE_YEAR
          ).keys(),
        ].map((monthsToAdd) => ({
          ...event,
          startDate: format(
            addMonths(parseISO(event.startDate), monthsToAdd),
            "yyyy-MM-dd"
          ),
          endDate: format(
            addMonths(parseISO(event.endDate), monthsToAdd),
            "yyyy-MM-dd"
          ),
        }));
      } else if (event.repeats === "yearly") {
        return [...Array(validation.NUM_YEARS_TO_REPEAT_EVENT).keys()].map(
          (yearsToAdd) => ({
            ...event,
            startDate: format(
              addYears(parseISO(event.startDate), yearsToAdd),
              "yyyy-MM-dd"
            ),
            endDate: format(
              addYears(parseISO(event.endDate), yearsToAdd),
              "yyyy-MM-dd"
            ),
          })
        );
      }
      return [event];
    });
};

export const getEventById = async (id) => {
  const snapshot = await get(ref(db, `events/${id}`));

  return {
    ...snapshot.val(),
    id,
  };
};

export const deleteEvent = async (eventId, username) => {
  return update(ref(db), {
    [`users/${username}/events/${eventId}`]: null,
    [`events/${eventId}`]: null,
  });
};

export const getAllUniqueEvents = async (username) => {
  const snapshot = await get(ref(db, "events"));

  if (!snapshot.exists()) {
    return [];
  }

  return Object.entries(snapshot.val())
    .map((entry) => {
      const [uid, event] = entry;
      return {
        id: uid,
        ...event,
      };
    })
    .filter((event) => event.username === username);
};

export const editEventTitle = async (eventId, title) => {
  return update(ref(db), {
    [`events/${eventId}/title`]: title,
  });
};

export const editEventAdditionalInfo = async (eventId, additionalInfo) => {
  return update(ref(db), {
    [`events/${eventId}/additionalInfo`]: additionalInfo,
  });
};
