import { ref, push, get, update } from "firebase/database";
import { db } from "../config/firebase-config";

export const createNotification = async (contacts, username, eventId) => {
  const arr = Object.entries(contacts);

  const filteredContacts = Object.keys(contacts)
    .filter((key) => !key.includes(`${username}`))
    .reduce((cur, key) => {
      return Object.assign(cur, { [key]: contacts[key] });
    }, {});

  const { key } = await push(ref(db, "notifications"), filteredContacts);

  arr.forEach((user) => {
    const currUsername = user[0];

    update(ref(db), {
      [`users/${currUsername}/notifications/${key}`]: eventId,
    });
  });
};

export const getAllNotifications = async (username) => {
  const snapshot = await get(ref(db, `users/${username}/notifications`));

  if (!snapshot.exists()) {
    return [];
  }

  return Object.entries(snapshot.val()).map((entry) => {
    const [notificationId, eventId] = entry;
    return {
      id: notificationId,
      eventId,
    };
  });
};
