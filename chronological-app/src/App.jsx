import Home from "./views/Home";
import Login from "./views/Login";
import Register from "./views/Register";
import { Route, Routes, useSearchParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./config/firebase-config";
import { getUserById } from "./services/users.service";
import Calendar from "./views/Calendar";
import Day from "./components/CalendarDaily/Day";
import Month from "./components/CalendarMonthly/Month";
import Week from "./components/CalendarWeekly/Week";
import AboutUs from "./views/AboutUs";
import Users from "./views/Users";
import Authenticated from "./components/Authenticated/Authenticated";
import { AppContext } from "./context/app.context";
import UserProfile from "./views/UserProfile";
import WorkWeek from "./components/CalendarWorkWeek/WorkWeek";
import AddEventForm from "./components/AddEventForm/AddEvent";
import MyContacts from "./views/MyContacts";
import AddContactList from "./components/ContactLists/AddContactList";
import SingleContactList from "./views/SingleContactList";
import PrivateHeader from "./components/Headers/PrivateHeader";
import SidebarNav from "./components/SidebarNav/SidebarNav";
import NotFound from "./views/NotFound";
import DetailedViewModal from "./components/Calendar/DetailedViewModal";
import MyEvents from "./views/MyEvents";
import AddContact from "./components/ContactLists/AddContact";

export default function App() {
  const [user, loading] = useAuthState(auth);

  const [appState, setAppState] = useState({
    user: user ? { email: user.email, uid: user.uid } : null,
    userData: null,
  });

  const [toasts, setToasts] = useState([]);

  useEffect(() => {
    setAppState({
      ...appState,
      user: user ? { email: user.email, uid: user.uid } : null,
    });
  }, [user]);

  useEffect(() => {
    if (appState.user !== null) {
      getUserById(appState.user.uid)
        .then((userData) =>
          userData
            ? setAppState({ ...appState, userData })
            : setAppState({ ...appState })
        )
        .catch((e) => console.log(e.message));
    }
  }, [appState.user]);

  const addToast = (type, message) => {
    const toast = {
      class: type === "error" ? "alert-error" : "alert-success",
      message,
    };

    setToasts((toasts) => [...toasts, toast]);

    setTimeout(
      () => setToasts((toasts) => toasts.filter((t) => t !== toast)),
      5000
    );
  };

  const [showModal, setShowModal] = useState(false);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  return (
    <AppContext.Provider value={{ ...appState, setAppState, addToast }}>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            path="/calendar"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <Calendar showModal={showModal} toggleModal={toggleModal} />
              </Authenticated>
            }
          >
            <Route path="month" element={<Month toggleModal={toggleModal} />}>
              <Route path="add-event" element={<AddEventForm />} />
            </Route>
            <Route path="week" element={<Week toggleModal={toggleModal} />}>
              <Route path=":uid" element={<DetailedViewModal />} />
              <Route path="add-event" element={<AddEventForm />} />
            </Route>
            <Route
              path="work-week"
              element={<WorkWeek toggleModal={toggleModal} />}
            >
              <Route path="add-event" element={<AddEventForm />} />
              <Route path=":uid" element={<DetailedViewModal />} />
            </Route>
            <Route path="day" element={<Day toggleModal={toggleModal} />}>
              <Route path="add-event" element={<AddEventForm />} />
              <Route path=":uid" element={<DetailedViewModal />} />
            </Route>
          </Route>
          <Route
            path="/my-contacts"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <MyContacts showModal={showModal} toggleModal={toggleModal} />
              </Authenticated>
            }
          >
            <Route
              path="add-contact-list"
              element={<AddContactList toggleModal={toggleModal} />}
            />
          </Route>

          <Route
            path="/my-contacts/:listId"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <SingleContactList
                  showModal={showModal}
                  toggleModal={toggleModal}
                />
              </Authenticated>
            }
          >
            <Route
              path="add-contact"
              element={<AddContact toggleModal={toggleModal} />}
            />
          </Route>

          <Route
            path="/users"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <Users />
              </Authenticated>
            }
          />
          <Route
            path="/about"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <AboutUs />
              </Authenticated>
            }
          />
          <Route
            path="/my-events"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <MyEvents />
              </Authenticated>
            }
          />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route
            path="/profile/:uid"
            element={
              <Authenticated user={appState.user} loading={loading}>
                <SidebarNav />
                <PrivateHeader />
                <UserProfile />
              </Authenticated>
            }
          />
          <Route path="/*" element={<NotFound />} />
        </Routes>
        <div className="toast">
          {toasts.map((t, i) => (
            <div key={i} className={`alert ${t.class}`}>
              <div>
                <span>{t.message}</span>
              </div>
            </div>
          ))}
        </div>
      </div>
    </AppContext.Provider>
  );
}
