import logo from "../../assets/images/Logo.png";
import { useNavigate } from "react-router-dom";

export default function PublicHeader() {
  const navigate = useNavigate();

  return (
    <div className="bg-indigo-600">
      <div className="w-full px-6 flex items-center justify-between border-b border-indigo-500 lg:border-none">
        <div className="flex items-center">
          <a
            className="cursor-pointer"
            onClick={() => {
              navigate("/");
            }}
          >
            <img className="h-20 w-auto cursor-pointer" src={logo} alt="logo" />
          </a>
        </div>
        <div className="ml-10 space-x-4">
          <a
            className="inline-block cursor-pointer bg-indigo-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
            onClick={() => {
              navigate("/login");
            }}
          >
            Login
          </a>
          <a
            className="inline-block bg-white py-2 px-4 border border-transparent rounded-md text-base font-medium text-black cursor-pointer hover:bg-pink-600"
            onClick={() => {
              navigate("/register");
            }}
          >
            Register
          </a>
        </div>
      </div>
    </div>
  );
}
