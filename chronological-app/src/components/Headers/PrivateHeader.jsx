import { Fragment, useContext, useState, useEffect } from 'react';
import { onValue, ref } from 'firebase/database';
import { db } from '../../config/firebase-config';
import {
  RiCalendarLine,
  RiCalendarEventLine,
  RiContactsBookLine,
  RiInformationLine,
  RiLogoutBoxRLine,
  RiLogoutCircleRLine,
} from 'react-icons/ri';
import { Dialog, Menu, Transition } from '@headlessui/react';
import { TbUsers } from 'react-icons/tb';
import WeatherCard from '../../components/WeatherCard/WeatherCard';
import Logo from '../../assets/images/Logo.png';
import AllNotifications from '../../components/AllNotifications/AllNotifications';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import { HiOutlineMenuAlt2, HiOutlineUserCircle } from 'react-icons/hi';
import { VscBellDot, VscBell } from 'react-icons/vsc';
import { AppContext } from '../../context/app.context';
import { logoutUser } from '../../services/auth.service';
import { useNavigate, Link } from 'react-router-dom';

const navigation = [
  { name: 'Calendar', navigate: '/calendar/month', icon: RiCalendarLine },
  { name: 'My Events', navigate: '/my-events', icon: RiCalendarEventLine },
  { name: 'Contact list', navigate: '/my-contacts', icon: RiContactsBookLine },
  { name: 'Users', navigate: '/users', icon: TbUsers },
  { name: 'About us', navigate: '/about', icon: RiInformationLine },
  { name: 'Logout', navigate: '/login', icon: RiLogoutBoxRLine },
];

export default function PrivateHeader() {
  const [active, setActive] = useState(1);
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [notificationsOpen, setNotificationsOpen] = useState(false);
  const [notificationsLength, setNotificationsLength] = useState([]);
  const { setAppState, ...appState } = useContext(AppContext);
  const navigate = useNavigate();

  useEffect(() => {
    const notificationsRef = ref(
      db,
      `users/${appState?.userData?.username}/notifications`
    );
    return onValue(notificationsRef, (snapshot) => {
      const data = snapshot.val();

      setNotificationsLength(Object.keys(data || {}));
    });
  }, [appState?.userData]);

  const logout = async () => {
    await logoutUser();

    setAppState({
      ...appState,
      user: null,
      userData: null,
    });
    navigate('/login');
  };

  return (
    <div>
      <Transition.Root show={sidebarOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 flex z-40 md:hidden"
          onClose={setSidebarOpen}
        >
          <Transition.Child
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-600 bg-opacity-75" />
          </Transition.Child>
          <Transition.Child
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
            <div className="h-full relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-gradient-to-b from-indigo-500 to-blue-400">
              <Transition.Child
                enter="ease-in-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in-out duration-300"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <div className="absolute top-0 right-0 -mr-12 pt-2">
                  <button
                    type="button"
                    className="ml-1 flex items-center justify-center h-10 w-10 rounded-full"
                    onClick={() => setSidebarOpen(false)}
                  >
                    <span className="sr-only">Close sidebar</span>
                    <AiOutlineCloseCircle
                      className="h-6 w-6 text-white"
                      aria-hidden="true"
                    />
                  </button>
                </div>
              </Transition.Child>
              <div className="flex-shrink-0 flex items-center px-4">
                <img className="h-8 w-auto" src={Logo} alt="ChronologiCal" />
                <div className="px-5 text-xl text-white">ChronologiCal</div>
              </div>
              <div className="mt-5 overflow-y-auto">
                <nav className="px-2 space-y-1">
                  {navigation.map((item, index) => (
                    <Link
                      key={index}
                      to={item.navigate}
                      onClick={() => {
                        setActive(index + 1);
                        item.name === 'Logout' ? { logout } : null;
                      }}
                      className={
                        active === index + 1
                          ? 'bg-indigo-800 text-white group flex items-center px-2 py-2 text-sm font-medium rounded-md'
                          : 'text-indigo-100 hover:bg-indigo-600 group flex items-center px-2 py-2 text-sm font-medium rounded-md'
                      }
                    >
                      <item.icon
                        className="mr-3 flex-shrink-0 h-7 w-7 text-white"
                        aria-hidden="true"
                      />
                      {item.name}
                    </Link>
                  ))}
                </nav>
              </div>
              <div className="flex-1 flex flex-col"></div>
              <WeatherCard />
            </div>
          </Transition.Child>
          <div className="flex-shrink-0 w-14" aria-hidden="true">
            {/* Dummy element to force sidebar to shrink to fit close icon */}
          </div>
        </Dialog>
      </Transition.Root>
      <div className="md:pl-64 flex flex-col flex-1">
        <div className="sticky top-0 z-0 flex-shrink-0 flex h-16 bg-white shadow">
          <button
            type="button"
            className="px-4 border-r border-gray-200 text-gray-500 md:hidden"
            onClick={() => setSidebarOpen(true)}
          >
            <span className="sr-only">Open sidebar</span>
            <HiOutlineMenuAlt2 className="h-6 w-6" aria-hidden="true" />
          </button>
          <div className="flex-1 px-4 flex justify-between">
            <div className="flex-1 flex"></div>
            <div className="ml-4 flex items-center md:ml-6">
              <button
                type="button"
                onClick={() => setNotificationsOpen(!notificationsOpen)}
                className="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500"
              >
                <span className="sr-only">View notifications</span>
                {notificationsLength.length === 0 ? (
                  <VscBell className="h-6 w-6" aria-hidden="true" />
                ) : (
                  <VscBellDot className="h-6 w-6" aria-hidden="true" />
                )}
              </button>
              {notificationsOpen && <AllNotifications />}
              {/* Profile dropdown */}
              <Menu as="div" className="ml-3 relative">
                <div className="flex items-center justify-between">
                  <div className="hidden mr-2 md:block">
                    {appState?.userData?.firstName}
                  </div>
                  <Menu.Button className="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2">
                    <span className="sr-only">Open user menu</span>
                    <div className="btn btn-ghost btn-circle avatar">
                      <img
                        className="rounded-full"
                        src={appState?.userData?.avatarUrl}
                        alt="avatar"
                      />
                    </div>
                  </Menu.Button>
                </div>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <Link
                      to={`/profile/${appState.user?.uid}`}
                      className="flex hover:bg-indigo-100 px-4 py-2 text-sm"
                    >
                      <HiOutlineUserCircle className="text-xl mr-1" />
                      My Profile
                    </Link>
                    <Link
                      onClick={logout}
                      className="flex hover:bg-indigo-100 px-4 py-2 text-sm"
                    >
                      <RiLogoutCircleRLine className="text-xl mr-1" />
                      Logout
                    </Link>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
