// import {Link} from 'react-router-dom';
import { useContext } from "react";
import { AppContext } from "../../context/app.context";
import { BiUserCircle, BiBlock } from "react-icons/bi";
import { Link } from "react-router-dom";

export default function UserCard({ user, changeRole }) {
  const { ...appState } = useContext(AppContext);

  const timeElapsed = user.registeredOn;
  const userDate = new Date(timeElapsed);

  function showBlockOrUnblockButton() {
    if (appState.userData?.role === "Admin") {
      if (user.role === "User") {
        return (
          <a
            className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium cursor-pointer border rounded-br-lg hover:text-gray-500"
            onClick={() => changeRole(user.username, "Blocked")}
          >
            <BiBlock className="w-5 h-5" aria-hidden="true" />
            <span className="ml-3">Block</span>
          </a>
        );
      } else if (user.role === "Blocked") {
        return (
          <a
            className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium cursor-pointer border rounded-br-lg hover:text-gray-500"
            onClick={() => changeRole(user.username, "User")}
          >
            <BiBlock className="w-5 h-5" aria-hidden="true" />
            <span className="ml-3">Unblock</span>
          </a>
        );
      }
    }
  }

  return (
    <li className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200">
      <div className="flex-1 flex flex-col p-8">
      <div className="flex avatar justify-center pb-5">
                  <div className="w-32 border-4 border-blue-400 rounded-full">
                    <img src={user?.avatarUrl} alt="avatar" />
                  </div>
                </div>
        <h3 className="mt-6 text-gray-900 text-sm font-medium">
          {user.username}
        </h3>
        <dl className="mt-1 flex-grow flex flex-col justify-between">
          <dd className="text-gray-500 text-sm">
            Since: {userDate.toDateString()}
          </dd>
          <dt className="sr-only">Role</dt>
          {user.role === "User" ? (
            <dd className="mt-3">
              <span className="px-2 py-1 text-white text-xs font-medium bg-blue-600 rounded-full">
                {user.role}
              </span>
            </dd>
          ) : null}
          {user.role === "Admin" ? (
            <dd className="mt-3">
              <span className="px-2 py-1 text-white text-xs font-medium bg-red-600 rounded-full">
                {user.role}
              </span>
            </dd>
          ) : null}
          {user.role === "Blocked" ? (
            <dd className="mt-3">
              <span className="px-2 py-1 text-white text-xs font-medium bg-orange-600 rounded-full">
                {user.role}
              </span>
            </dd>
          ) : null}
        </dl>
      </div>
      <div>
        <div className="-mt-px flex divide-x divide-gray-200">
          <div className="w-0 flex-1 flex">
            <Link
              to={`/profile/${user.uid}`}
              className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium cursor-pointer border rounded-bl-lg hover:text-gray-500"
            >
              <BiUserCircle className="w-5 h-5" aria-hidden="true" />
              <span className="ml-3">View profile</span>
            </Link>
          </div>
          {showBlockOrUnblockButton()}
        </div>
      </div>
    </li>
  );
}
