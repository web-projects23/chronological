import { useNavigate } from "react-router-dom";
import { deleteEvent } from "../../services/events.service";

export default function DeleteEventModal({ eventId, username }) {
  const navigate = useNavigate();

  return (
    <div>
      <input type="checkbox" id="my-modal-5" className="modal-toggle" />
      <div className="modal">
        <div className="modal-box w-11/12 max-w-5xl">
          <div className="flex justify-center">
            <svg
              className="w-10 h-10 fill-current text-red-500"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
            >
              <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z" />
            </svg>
            <h3 className="font-bold text-lg m-3 self-center">
              Are you sure you want to delete this event?
            </h3>
          </div>
          <div className="modal-action flex justify-center">
            <label htmlFor="my-modal-5" className="btn">
              Cancel
            </label>
            <label
              htmlFor="my-modal-5"
              className="btn btn-error"
              onClick={() => {
                deleteEvent(eventId, username);
                navigate("/my-events");
              }}
            >
              Delete
            </label>
          </div>
        </div>
      </div>
    </div>
  );
}
