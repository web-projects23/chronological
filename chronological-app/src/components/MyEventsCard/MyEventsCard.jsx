import { useContext, useState } from "react";
import { AiOutlineCalendar, AiFillEdit } from "react-icons/ai";
import { IoMdTime } from "react-icons/io";
import { useNavigate } from "react-router-dom";
import { AppContext } from "../../context/app.context";
import {
  editEventAdditionalInfo,
  editEventTitle,
} from "../../services/events.service";
import DeleteEventModal from "./DeleteEventModal";
import { RiUser3Fill } from "react-icons/ri";

export default function MyEventsCard({ event }) {
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const navigate = useNavigate();
  const [state, setState] = useState({
    event: event,
    showEditTitle: false,
    showEditAdditionalInfo: false,
    titleText: "",
    additionalInfoText: "",
  });
  const { addToast } = useContext(AppContext);
  const participants = event.length !== 0 && Object.keys(event?.contacts);

  const saveName = async () => {
    if (state.titleText === "" || state.titleText === state.event.title) {
      return setState({ ...state, showEditTitle: false });
    }

    try {
      await editEventTitle(state.event.id, state.titleText);
      addToast("success", "Event title was updated successfully!");

      setState({
        ...state,
        showEditTitle: false,
        event: {
          ...state.event,
          title: state.titleText,
        },
      });
    } catch (e) {
      addToast("error", e.message);
    }
  };

  const saveContent = async () => {
    if (
      state.additionalInfoText === "" ||
      state.additionalInfoText === state.event.additionalInfo
    ) {
      return setState({ ...state, showEditAdditionalInfo: false });
    }

    try {
      await editEventAdditionalInfo(state.event.id, state.additionalInfoText);
      addToast("success", "Event content was updated successfully!");

      setState({
        ...state,
        showEditAdditionalInfo: false,
        event: {
          ...state.event,
          additionalInfo: state.additionalInfoText,
        },
      });
    } catch (e) {
      addToast("error", e.message);
    }
  };

  const colorClasses = (event) => {
    switch (event?.category) {
      case "pink":
        return "bg-pink-100";
      case "purple":
        return "bg-purple-100";
      case "blue":
        return "bg-blue-100";
      case "green":
        return "bg-green-100";
      case "orange":
        return "bg-orange-100";
      case "yellow":
        return "bg-yellow-100";
      case "red":
        return "bg-red-100";
    }
  };

  return (
    <li className="mb-10 ml-6">
      <span className="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white">
        <svg
          aria-hidden="true"
          className="w-3 h-3 text-blue-600 "
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
            clipRule="evenodd"
          ></path>
        </svg>
      </span>
      <h3 className="flex items-center mb-1 text-lg font-semibold text-gray-900 ">
        {event.title}
        <span
          className={
            "text-black text-sm font-medium mr-2 px-2.5 py-0.5 rounded ml-3 " +
            colorClasses(event)
          }
        >
          {event.repeats ? event.repeats : "no repeats"}
        </span>

        <button
          className="btn btn-ghost m-2"
          onClick={() =>
            setState({ ...state, showEditTitle: !state.showEditTitle })
          }
        >
          <AiFillEdit />
        </button>
        {state.showEditTitle && (
          <input
            className="input w-40 bg-slate-200"
            defaultValue={state.event.title}
            onChange={(e) => setState({ ...state, titleText: e.target.value })}
            type="text"
            title="title"
          />
        )}
        <button
          className={`btn btn-ghost ml-2 ${
            state.showEditTitle ? "" : "hidden"
          }`}
          onClick={() => {
            saveName();
            navigate("/my-events");
          }}
        >
          Save
        </button>
      </h3>
      <time className="block mb-2 text-sm font-normal leading-none text-gray-400">
        <div className="flex mb-2">
          <span className="flex items-center justify-center mr-2">
            <AiOutlineCalendar className="mr-1" />
            {event?.startDate}
          </span>
          <span className="flex items-center justify-center">
            <IoMdTime className="mr-1" />
            {event?.startTime}
          </span>
        </div>
      </time>
      <p className="text-gray-400 text-sm"> Creator: {event.username}</p>
      <p className="mb-3 text-base font-normal text-gray-500">
        Details: {event.additionalInfo}
        <button
          className="btn btn-ghost"
          onClick={() =>
            setState({
              ...state,
              showEditAdditionalInfo: !state.showEditAdditionalInfo,
            })
          }
        >
          <AiFillEdit className="text-gray-700" />
        </button>
        {state.showEditAdditionalInfo && (
          <textarea
            className="input w-80 h-15 bg-slate-200 break-words"
            defaultValue={state.event.additionalInfo}
            onChange={(e) =>
              setState({ ...state, additionalInfoText: e.target.value })
            }
            type="text"
            name="name"
          ></textarea>
        )}
        <button
          className={`btn btn-ghost ${
            state.showEditAdditionalInfo ? "" : "hidden"
          }`}
          onClick={() => {
            saveContent();
            navigate("/my-events");
          }}
        >
          Save
        </button>
      </p>
      <h3 className="text-md font-extrabold text-gray-500 mb-1">
        Participants:
      </h3>
      <ol className="text-gray-700 text-base mb-4">
        {participants &&
          participants.map((p, i) => (
            <div key={i} className="flex leading-5 mb-1">
              <RiUser3Fill className="mr-1" />
              <div className="text-md text-gray-700">{p}</div>
            </div>
          ))}
      </ol>

      <label
        htmlFor="my-modal-5"
        className="cursor-pointer inline-flex items-center py-1 px-2 text-sm font-medium text-gray-900 bg-red-400 rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:outline-none focus:ring-gray-200 focus:text-blue-700"
        onClick={() => {
          setShowDeleteDialog(true);
        }}
      >
        Delete
      </label>
      {showDeleteDialog ? (
        <DeleteEventModal eventId={event.id} username={event.username} />
      ) : null}
    </li>
  );
}
