import { useContext, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { loginUser } from "../../services/auth.service";
import { getUserById } from "../../services/users.service";
import { AppContext } from "../../context/app.context";

export default function LoginForm() {
  const { addToast, setAppState, ...appState } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();

  const emailRegEx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  const passwordRegEx =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,30}$/g;

  const [form, setForm] = useState({
    email: {
      value: "",
      touched: false,
    },
    password: {
      value: "",
      touched: false,
    },
  });

  const updateEmail = (value = "") => {
    setForm({
      ...form,
      email: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const updatePassword = (value = "") => {
    setForm({
      ...form,
      password: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const validateEmail = emailRegEx.test(form.email.value);
  const validatePassword = passwordRegEx.test(form.password.value);

  const login = async () => {
    if (!validateEmail || !validatePassword) {
      return addToast("error", "Invalid email or password");
    }
    try {
      const credentials = await loginUser(
        form.email.value,
        form.password.value
      );

      setAppState({
        ...appState,
        user: {
          email: credentials.user.email,
          uid: credentials.user.uid,
        },
      });

      getUserById(credentials.user.uid).then((user) => {
        if (user.role !== "Blocked") {
          addToast("success", "You've logged in successfully");
          navigate(location?.state?.from?.pathname || "/calendar/month");
        } else {
          addToast("error", "You are blocked!");
          logoutUser();
          return;
        }
      });
    } catch (error) {
      if (error.message.includes("auth/invalid-email")) {
        return addToast("error", "Please enter a valid email");
      }
      if (error.message.includes("auth/wrong-password")) {
        return addToast("error", "Wrong password");
      }
      if (error.message.includes("auth/user-not-found")) {
        return addToast("error", "No user with that email");
      }

      addToast("error", "Something went wrong");
    }
  };

  return (
    <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
      <div className="relative py-3 sm:max-w-xl sm:mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-indigo-300 to-indigo-600 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
        <div className="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
          <div className="max-w-md mx-auto">
            <div>
              <h1 className="text-2xl font-semibold">Login</h1>
            </div>
            <div className="divide-y divide-gray-200">
              <div className="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                <div className="relative">
                  <input
                    value={form.email.value}
                    onChange={(e) => updateEmail(e.target.value)}
                    id="email"
                    name="email"
                    type="email"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Email address"
                  />
                  <label
                    htmlFor="email"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Email Address
                  </label>
                  <br />
                  <label>
                    {form.email.touched && !validateEmail && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid email"}
                      </span>
                    )}
                  </label>
                </div>
                <div className="relative">
                  <input
                    value={form.password.value}
                    onChange={(e) => updatePassword(e.target.value)}
                    id="password"
                    name="password"
                    type="password"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Password"
                  />
                  <label
                    htmlFor="password"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Password
                  </label>
                  <br />
                  <label>
                    {form.password.touched && !validatePassword && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid password"}
                      </span>
                    )}
                  </label>
                </div>
                <div className="flex flex-col items-center relative">
                  <button
                    className="btn btn-outline bg-indigo-500 text-white w-2/3 rounded-md px-2 py-1"
                    onClick={login}
                  >
                    Login
                  </button>
                  <p className="mt-4 text-sm">
                    You don`t have an account?{" "}
                    <span
                      className="underline cursor-pointer"
                      onClick={() => navigate("/register")}
                    >
                      Sign up
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
