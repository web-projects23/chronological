import { useState, useEffect, useContext } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { validation } from "../../common/validation-enum";
import { AppContext } from "../../context/app.context";
import { addContactList } from "../../services/contact-list.service";
import MyCombobox from "../ComboBox/CBAddContactList";
import { allUsers } from "../../services/users.service";

export default function AddContactList({ toggleModal }) {
  const { userData } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();

  const [users, setUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [searchedName, setSearchedName] = useState("");

  useEffect(() => {
    allUsers().then(setUsers).catch(console.error);
  }, []);

  const previousLocation = location.pathname.substring(
    0,
    location.pathname.lastIndexOf("/")
  );

  const newContactListInitState = {
    listName: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    contacts: [],
  };

  const [form, setForm] = useState(newContactListInitState);

  const updateListName = (value = "") => {
    setForm({
      ...form,
      listName: {
        value,
        touched: value === "" ? false : true,
        valid:
          value.length >= validation.MIN_TITLE_LENGTH &&
          value.length <= validation.MAX_TITLE_LENGTH,
        error:
          value.length < validation.MIN_TITLE_LENGTH
            ? `Minimum title length: ${validation.MIN_TITLE_LENGTH}`
            : `Maximum title length: ${validation.MAX_TITLE_LENGTH}`,
      },
    });
  };

  useEffect(() => {
    form.contacts = [...selectedUsers];
  }, [selectedUsers]);

  const createContactList = async (e) => {
    if (e.type === "submit") {
      e.preventDefault();
    }
    if (!form.listName.valid && e.type === "click") {
      e.preventDefault();
    }
    if (!form.listName.valid) return addToast("error", "Invalid name!");

    const listUsersArr = form.contacts.map((el) => ({ [el.username]: el }));
    const listUsersObj = Object.assign({}, ...listUsersArr);

    let newId;
    try {
      newId = await addContactList(
        form.listName.value,
        userData.username,
        listUsersObj
      );
      setForm(newContactListInitState);
      setSelectedUsers([]);
      navigate(previousLocation);
      toggleModal();
    } catch (e) {
      console.log(e);
      addToast("error", e.message);
    }
  };

  const clearAndHideModal = () => {
    setForm(newContactListInitState);
    setSelectedUsers([]);
    navigate(previousLocation);
    toggleModal();
  };

  return (
    <div className="modal modal-bottom sm:modal-middle rounded-[0px]">
      <div className="modal-box rounded-[0] lg:rounded-[0]">
        {/* Name */}
        <div className="form-control w-full max-w-xs ">
          <label className="label">
            <span className="label-text">List Name</span>
          </label>
          <input
            type="text"
            placeholder="List name"
            className="input input-bordered w-full max-w-xs"
            value={form.listName.value}
            onChange={(e) => updateListName(e.target.value)}
          />
          <label>
            {form.listName.touched && !form.listName.valid && (
              <span className="label-text-alt text-red-400">
                {form.listName.error}
              </span>
            )}
          </label>
        </div>

        <MyCombobox
          users={users}
          selectedUsers={selectedUsers}
          setSelectedUsers={setSelectedUsers}
          searchedName={searchedName}
          setSearchedName={setSearchedName}
        />

        <div className="flex flex-row-reverse gap-2 mt-5">
          <button onClick={createContactList}>
            <label className="btn min-w-auto w-32 h-10 bg-blue-300 p-2 rounded-xl hover:bg-blue-500 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold">
              Create list
            </label>
          </button>

          <label
            onClick={clearAndHideModal}
            className="btn min-w-auto w-32 h-10 bg-gray-500 p-2 rounded-xl hover:bg-gray-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
          >
            Cancel
          </label>
        </div>
      </div>
    </div>
  );
}
