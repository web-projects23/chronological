import { useState, useEffect, useContext } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { AppContext } from "../../context/app.context";
import {
  getContactListById,
  addContact,
} from "../../services/contact-list.service";
import { allUsers } from "../../services/users.service";
import AddContactComboBox from "../ComboBox/CBAddContact";

export default function AddContact({ toggleModal }) {
  const { addToast } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();
  const { listId } = useParams();

  const [contacts, setContacts] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [searchedName, setSearchedName] = useState("");

  useEffect(() => {
    allUsers().then(setContacts).catch(console.error);
  }, []);

  useEffect(() => {
    getContactListById(listId)
      .then((data) => setSelectedUsers(Object.values(data[0])))
      .catch(console.error);
  }, [location]);

  const previousLocation = location.pathname.substring(
    0,
    location.pathname.lastIndexOf("/")
  );

  const newContactListInitState = {
    listName: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    contacts: [],
  };

  const [form, setForm] = useState(newContactListInitState);

  useEffect(() => {
    form.contacts = Object.values(selectedUsers);
  }, [selectedUsers]);

  const addContactToList = async (e) => {
    if (e.type === "submit") {
      e.preventDefault();
    }
    const listUsersArr = form.contacts.map((el) => ({ [el.username]: el }));
    const listUsersObj = Object.assign({}, ...listUsersArr);

    let newId;
    try {
      newId = await addContact(listId, listUsersObj);
      setForm(newContactListInitState);
      setSelectedUsers([]);
      navigate(previousLocation);
      toggleModal();
    } catch (e) {
      console.log(e);
      addToast("error", e.message);
    }
  };

  const clearAndHideModal = () => {
    setForm(newContactListInitState);
    setSelectedUsers([]);
    navigate(previousLocation);
    toggleModal();
  };

  return (
    <div className="modal modal-bottom sm:modal-middle rounded-[0px]">
      <div className="modal-box rounded-[0] lg:rounded-[0]">
        <AddContactComboBox
          contacts={contacts}
          selectedUsers={selectedUsers}
          setSelectedUsers={setSelectedUsers}
          searchedName={searchedName}
          setSearchedName={setSearchedName}
        />

        <div className="flex flex-row-reverse gap-2 mt-5">
          <button onClick={addContactToList}>
            <label className="btn min-w-auto w-32 h-10 bg-blue-300 p-2 rounded-xl hover:bg-blue-500 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold">
              Add Contact
            </label>
          </button>

          <label
            onClick={clearAndHideModal}
            className="btn min-w-auto w-32 h-10 bg-gray-500 p-2 rounded-xl hover:bg-gray-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
          >
            Cancel
          </label>
        </div>
      </div>
    </div>
  );
}
