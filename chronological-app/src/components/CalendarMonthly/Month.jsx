import { useContext, useEffect, useState } from "react";
import { addMonths, endOfMonth, parseISO, startOfMonth } from "date-fns";
import WeekDays from "./WeekDays";
import MonthDays from "./MonthDays";
import CurrentMonthHeader from "./CurrentMonthHeader";
import { getAllEvents } from "../../services/events.service";
import { useLocation } from "react-router-dom";
import { AppContext } from "../../context/app.context";

export default function Month({ showModal, toggleModal }) {
  const { userData } = useContext(AppContext);
  const currentDate = new Date();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [monthOffset, setMonthOffset] = useState(0);
  const [events, setEvents] = useState([]);
  const location = useLocation();
  const monthInView = addMonths(currentDate, monthOffset);
  const firstDayOfMonth = startOfMonth(monthInView, { weekStartsOn: 1 });
  const lastDayOfMonth = endOfMonth(monthInView, { weekStartsOn: 1 });

  const nextMonth = () => {
    setMonthOffset(monthOffset + 1);
  };
  const prevMonth = () => {
    setMonthOffset(monthOffset - 1);
  };
  const onDateClick = (date) => {
    setSelectedDate(date);
  };
  const resetSelectedDateToCurrentDate = () => {
    setMonthOffset(0);
    setSelectedDate(currentDate);
  };

  useEffect(() => {
    getAllEvents(userData?.username)
      .then((events) => {
        setEvents(
          events.filter((event) => {
            return (
              parseISO(event.startDate) >= firstDayOfMonth &&
              parseISO(event.startDate) <= lastDayOfMonth
            );
          })
        );
      })
      .catch(console.error);
  }, [location, monthOffset]);

  return (
    <div className="md:pl-64">
      <CurrentMonthHeader
        monthInView={addMonths(currentDate, monthOffset)}
        prevMonth={prevMonth}
        nextMonth={nextMonth}
        resetSelectedToCurrentDate={resetSelectedDateToCurrentDate}
        toggleModal={toggleModal}
        showModal={showModal}
      />

      <div className="bg-white text-gray-700 overflow-auto">
        <WeekDays currentDate={currentDate} />
        <MonthDays
          currentDate={currentDate}
          monthInView={addMonths(currentDate, monthOffset)}
          selectedDate={selectedDate}
          onDateClick={onDateClick}
          events={events}
        />
      </div>
    </div>
  );
}
