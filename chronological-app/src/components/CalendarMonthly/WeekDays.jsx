import startOfWeek from "date-fns/startOfWeek";
import { format, addDays } from "date-fns";

export default function WeekDays({ currentDate }) {
  const dateFormat = "E";
  const days = [];
  let startDate = startOfWeek(currentDate, { weekStartsOn: 1 });

  for (let i = 0; i < 7; i++) {
    days.push(<div key={i}>{format(addDays(startDate, i), dateFormat)}</div>);
  }

  return (
    <div className="grid grid-cols-7 gap-3 justify-between text-center p-5 sticky border-b border-solid border-gray-200 z-2">
      {days}
    </div>
  );
}
