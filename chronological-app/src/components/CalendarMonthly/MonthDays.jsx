import {
  format,
  startOfWeek,
  endOfWeek,
  addDays,
  startOfMonth,
  endOfMonth,
  isSameMonth,
  isSameDay,
} from "date-fns";
import { useNavigate } from "react-router-dom";
import { GrPlan } from "react-icons/gr";

export default function MonthDays({
  currentDate,
  monthInView,
  selectedDate,
  onDateClick,
  events,
}) {
  const uniqueEventsDates = new Set(
    events.flatMap((event) => [event.startDate, event.endDate])
  );
  const monthStart = startOfMonth(monthInView);
  const monthEnd = endOfMonth(monthStart);
  const startDate = startOfWeek(monthStart, { weekStartsOn: 1 });
  const endDate = endOfWeek(monthEnd, { weekStartsOn: 1 });
  const navigate = useNavigate();

  const rows = [];
  let days = [];
  let day = startDate;

  while (day <= endDate) {
    for (let i = 0; i < 7; i++) {
      const cloneDay = day;
      days.push(
        <div
          className={`p-2 w-10 sm:p-6 sm:w-24 justify-self-center hover:border hover:border-solid ${
            !isSameMonth(day, monthStart)
              ? " pointer-events-none text-gray-300"
              : ""
          }${
            isSameDay(day, selectedDate)
              ? "rounded-full bg-violet-500 text-white"
              : ""
          }${
            isSameMonth(currentDate, monthInView) && isSameDay(day, monthInView)
              ? "rounded-full bg-sky-500 text-white"
              : ""
          }`}
          key={day}
          onClick={() => onDateClick(cloneDay)}
          onDoubleClick={() =>
            navigate("/calendar/day", { state: { date: cloneDay } })
          }
        >
          <span className="text-lg">{format(day, "d")}</span>
          {uniqueEventsDates.has(format(day, "yyyy-MM-dd")) && (
            <GrPlan className="z-1" />
          )}
        </div>
      );
      day = addDays(day, 1);
    }
    rows.push(
      <div
        className="grid grid-cols-7 text-center p-2 h-36 items-center border-b border-solid border-gray-200"
        key={day}
      >
        {days}
      </div>
    );
    days = [];
  }
  return <div>{rows}</div>;
}
