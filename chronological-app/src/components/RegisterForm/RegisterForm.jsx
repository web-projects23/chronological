import { AppContext } from "../../context/app.context";
import { useNavigate } from "react-router-dom";
import { useContext, useState } from "react";
import { registerUser, loginUser } from "../../services/auth.service";
import {
  createUser,
  getUser,
  getUserByPhone,
} from "../../services/users.service";

export default function RegisterForm() {
  const { addToast, setAppState, ...appState } = useContext(AppContext);
  const navigate = useNavigate();

  const usernameRegEx =
    /^(?=.{3,30}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
  const emailRegEx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  const phoneRegEx = /^[0-9]{10}$/g;
  const passwordRegEx =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,30}$/g;
  const firstNameRegEx = /^[A-Za-z]{1,30}$/g;
  const lastNameRegEx = /^[A-Za-z]{1,30}$/g;

  const [form, setForm] = useState({
    username: {
      value: "",
      touched: false,
    },
    email: {
      value: "",
      touched: false,
    },
    password: {
      value: "",
      touched: false,
    },
    firstName: {
      value: "",
      touched: false,
    },
    lastName: {
      value: "",
      touched: false,
    },
    phoneNumber: {
      value: "",
      touched: false,
    },
  });

  const updateUsername = (value = "") => {
    setForm({
      ...form,
      username: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const updateEmail = (value = "") => {
    setForm({
      ...form,
      email: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const updatePassword = (value = "") => {
    setForm({
      ...form,
      password: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };
  const updatePhoneNum = (value = "") => {
    setForm({
      ...form,
      phoneNumber: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const updateFirstName = (value = "") => {
    setForm({
      ...form,
      firstName: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const updateLastName = (value = "") => {
    setForm({
      ...form,
      lastName: {
        value,
        touched: value === "" ? false : true,
      },
    });
  };

  const validateUsername = usernameRegEx.test(form.username.value);
  const validateEmail = emailRegEx.test(form.email.value);
  const validatePassword = passwordRegEx.test(form.password.value);
  const validatePhoneNum = phoneRegEx.test(form.phoneNumber.value);
  const validateFirstName = firstNameRegEx.test(form.firstName.value);
  const validateLastName = lastNameRegEx.test(form.lastName.value);

  const register = async () => {
    if (!validateUsername) return addToast("error", "Invalid username");
    if (!validateEmail) return addToast("error", "Invalid email");
    if (!validatePassword) return addToast("error", "Invalid password");
    if (!validatePhoneNum) return addToast("error", "Invalid phone number");
    if (!validateFirstName) return addToast("error", "Invalid first name");
    if (!validateLastName) return addToast("error", "Invalid last name");

    try {
      const user = await getUser(form.username.value);
      const phoneNum = await getUserByPhone(form.phoneNumber.value);

      if (user !== null)
        return addToast("error", `User with this username already exists!`);
      if (phoneNum !== null)
        return addToast("error", `User with this phone number already exists!`);

      const credentials = await registerUser(
        form.email.value,
        form.password.value
      );

      try {
        const userData = await createUser(
          credentials.user.uid,
          form.firstName.value,
          form.lastName.value,
          form.username.value,
          form.phoneNumber.value,
          credentials.user.email
        );

        setAppState({
          ...appState,
          userData,
        });
      } catch (error) {
        return addToast("error", e.message);
      }

      try {
        const credentials = await loginUser(
          form.email.value,
          form.password.value
        );

        setAppState({
          ...appState,
          user: {
            email: credentials.user.email,
            uid: credentials.user.uid,
          },
        });
        navigate("/calendar/month");
        addToast("success", "You've logged in successfully");
      } catch (error) {
        addToast("error", "Something went wrong");
      }
    } catch (error) {
      if (error.message.includes("auth/email-already-in-use")) {
        return addToast("error", "Email has already been used");
      }

      if (error.message.includes("auth/invalid-email")) {
        return addToast("error", "Please enter valid email");
      }
      console.log(error.message);
      addToast("error", "Something went wrong");
    }
  };

  return (
    <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
      <div className="relative py-3 sm:max-w-xl sm:mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-indigo-300 to-indigo-600 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
        <div className="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
          <div className="max-w-md mx-auto">
            <div>
              <h1 className="text-2xl font-semibold">Register</h1>
            </div>
            <div className="divide-y divide-gray-200">
              <div className="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                <div className="relative">
                  <input
                    value={form.username.value}
                    onChange={(e) => updateUsername(e.target.value)}
                    id="username"
                    name="username"
                    type="text"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Username"
                  />
                  <label
                    htmlFor="username"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Username
                  </label>
                  <br />
                  <label>
                    {form.username.touched && !validateUsername && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid username"}
                      </span>
                    )}
                  </label>
                </div>
                <div className="relative">
                  <input
                    value={form.email.value}
                    onChange={(e) => updateEmail(e.target.value)}
                    id="email"
                    name="email"
                    type="email"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Email address"
                  />
                  <label
                    htmlFor="email"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Email Address
                  </label>
                  <br />
                  <label>
                    {form.email.touched && !validateEmail && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid email"}
                      </span>
                    )}
                  </label>
                </div>
                <div className="relative">
                  <input
                    value={form.password.value}
                    onChange={(e) => updatePassword(e.target.value)}
                    id="password"
                    name="password"
                    type="password"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Password"
                  />
                  <label
                    htmlFor="password"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Password
                  </label>
                  <br />
                  <label>
                    {form.password.touched && !validatePassword && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid password"}
                      </span>
                    )}
                  </label>
                </div>

                <div className="relative">
                  <input
                    value={form.phoneNumber.value}
                    onChange={(e) => updatePhoneNum(e.target.value)}
                    id="phone"
                    name="phone"
                    type="text"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Phone Number"
                  />
                  <label
                    htmlFor="phone"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Phone Number
                  </label>
                  <br />
                  <label>
                    {form.phoneNumber.touched && !validatePhoneNum && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid phone number"}
                      </span>
                    )}
                  </label>
                </div>

                <div className="relative">
                  <input
                    value={form.firstName.value}
                    onChange={(e) => updateFirstName(e.target.value)}
                    id="first-name"
                    name="first-name"
                    type="text"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="First Name"
                  />
                  <label
                    htmlFor="first-name"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    First Name
                  </label>
                  <br />
                  <label>
                    {form.firstName.touched && !validateFirstName && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid name"}
                      </span>
                    )}
                  </label>
                </div>

                <div className="relative">
                  <input
                    value={form.lastName.value}
                    onChange={(e) => updateLastName(e.target.value)}
                    id="last-name"
                    name="last-name"
                    type="text"
                    className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                    placeholder="Last Name"
                  />
                  <label
                    htmlFor="last-name"
                    className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                  >
                    Last Name
                  </label>
                  <br />
                  <label>
                    {form.lastName.touched && !validateLastName && (
                      <span className="w-full label-text-alt text-red-400">
                        {"Invalid name"}
                      </span>
                    )}
                  </label>
                </div>

                <div className="flex flex-col items-center relative">
                  <button
                    className="btn btn-outline bg-indigo-500 text-white w-2/3 rounded-md px-2 py-1"
                    onClick={register}
                  >
                    Create Account
                  </button>
                  <p className="mt-4 text-sm">
                    Already Have An Account?{" "}
                    <span
                      className="underline cursor-pointer"
                      onClick={() => navigate("/login")}
                    >
                      Click here to login
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
