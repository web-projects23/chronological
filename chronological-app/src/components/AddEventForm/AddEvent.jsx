import { format, parseISO } from "date-fns";
import { useContext, useState } from "react";
import { validation } from "../../common/validation-enum";
import { AppContext } from "../../context/app.context";
import { addEvent } from "../../services/events.service";
import { SlLoop, SlTag } from "react-icons/sl";
import { useLocation, useNavigate } from "react-router-dom";
import MyComboboxEvents from "../ComboBox/CBAddEventUsers";
import { useEffect } from "react";
import { allUsers } from "../../services/users.service";
import { createNotification } from "../../services/notifications.services";

export default function AddEventForm({ toggleModal }) {
  const currentDate = new Date();
  const formattedDate = format(currentDate, "yyyy-MM-dd");
  const formattedStartTime = format(currentDate, "HH:mm");
  const formattedEndTime = format(currentDate, "HH:mm");
  const { addToast, userData } = useContext(AppContext);
  const [users, setUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [searchedName, setSearchedName] = useState("");
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    allUsers().then(setUsers).catch(console.error);
  }, []);

  useEffect(() => {
    setSelectedUsers([{ ...userData }]);
  }, [userData]);

  const previousLocation = location.pathname.substring(
    0,
    location.pathname.lastIndexOf("/")
  );

  const newEventInitialState = {
    title: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    startDate: {
      value: formattedDate,
      touched: false,
    },
    startTime: {
      value: formattedStartTime,
      touched: false,
    },
    endDate: {
      value: formattedDate,
      touched: false,
    },
    endTime: {
      value: formattedEndTime,
      touched: false,
    },
    additionalInfo: {
      value: "",
      touched: false,
      valid: false,
      error: "",
    },
    category: {
      value: "blue",
    },
    repeats: {
      value: "",
    },
    privateCheckbox: {
      value: "",
    },
    contacts: [],
  };

  const [form, setForm] = useState(newEventInitialState);

  useEffect(() => {
    form.contacts = [...selectedUsers];
  }, [selectedUsers]);

  const updateTitle = (value = "") => {
    setForm({
      ...form,
      title: {
        value,
        touched: value === "" ? false : true,
        valid:
          value.length >= validation.MIN_TITLE_LENGTH &&
          value.length <= validation.MAX_TITLE_LENGTH,
        error:
          value.length < validation.MIN_TITLE_LENGTH
            ? "Minimum title length: 3"
            : "Maximum title length: 30",
      },
    });
  };

  const updateStartTimeAndEndTime = (newDateTime) => {
    setForm({
      ...form,
      startDate: {
        value: format(newDateTime, "yyyy-MM-dd"),
      },
      startTime: {
        value: format(newDateTime, "HH:mm"),
      },
      endDate: {
        value: format(newDateTime, "yyyy-MM-dd"),
      },
      endTime: {
        value: format(newDateTime, "HH:mm"),
      },
    });
  };

  const startTimeIsAfterEndTime = (startDateTime, endDateTime) => {
    return startDateTime > endDateTime;
  };

  const updateStartDate = (value = "") => {
    const newStartDateTime = parseISO(`${value}T${form.startTime.value}`);
    const endDateTime = parseISO(`${form.endDate.value}T${form.endTime.value}`);

    if (startTimeIsAfterEndTime(newStartDateTime, endDateTime)) {
      updateStartTimeAndEndTime(newStartDateTime);
    } else {
      setForm({
        ...form,
        startDate: {
          value,
          touched: value === "" ? false : true,
        },
        endDate: {
          value,
          touched: value === "" ? false : true,
        },
      });
    }
  };

  const updateEndDate = (value = "") => {
    const startDateTime = parseISO(
      `${form.startDate.value}T${form.startTime.value}`
    );
    const newEndDateTime = parseISO(`${value}T${form.endTime.value}`);

    if (startTimeIsAfterEndTime(startDateTime, newEndDateTime)) {
      updateStartTimeAndEndTime(newEndDateTime);
    } else {
      setForm({
        ...form,
        endDate: {
          value,
          touched: value === "" ? false : true,
        },
      });
    }
  };

  const updateStartTime = (value = "") => {
    const newStartDateTime = parseISO(`${form.startDate.value}T${value}`);
    const endDateTime = parseISO(`${form.endDate.value}T${form.endTime.value}`);

    if (startTimeIsAfterEndTime(newStartDateTime, endDateTime)) {
      updateStartTimeAndEndTime(newStartDateTime);
    } else {
      setForm({
        ...form,
        startTime: {
          value,
          touched: value === "" ? false : true,
        },
      });
    }
  };

  const updateEndTime = (value = "") => {
    const startDateTime = parseISO(
      `${form.startDate.value}T${form.startTime.value}`
    );
    const newEndDateTime = parseISO(`${form.endDate.value}T${value}`);

    if (startTimeIsAfterEndTime(startDateTime, newEndDateTime)) {
      updateStartTimeAndEndTime(newEndDateTime);
    } else {
      setForm({
        ...form,
        endTime: {
          value,
          touched: value === "" ? false : true,
        },
      });
    }
  };

  const updateAdditionalInfo = (value = "") => {
    setForm({
      ...form,
      additionalInfo: {
        value,
        touched: value === "" ? false : true,
        valid: value.length <= validation.MAX_ADDITIONAL_INFO_LENGTH,
        error:
          value.length > validation.MAX_ADDITIONAL_INFO_LENGTH &&
          "Maximum title length: 500",
      },
    });
  };

  const togglePrivateCheckbox = () => {
    setForm({
      ...form,
      privateCheckbox: {
        value: form.privateCheckbox.value === "checked" ? "" : "checked",
      },
    });
  };

  const handleCategoryChange = (changeEvent) => {
    setForm({
      ...form,
      category: {
        value: changeEvent.target.value,
      },
    });
  };

  const handleRepeatsChange = (changeEvent) => {
    setForm({
      ...form,
      repeats: {
        value: changeEvent.target.value,
      },
    });
  };

  const submitEvent = async (e) => {
    if (e.type === "submit") {
      e.preventDefault();
    }

    const invalidTitle = !form.title.valid;
    const invalidStartTime = !form.startTime.value;
    const invalidEndTime = !form.endTime.value;

    const hasErrors = invalidTitle || invalidStartTime || invalidEndTime;
    if (hasErrors && e.type === "click") {
      e.preventDefault();
    }

    if (invalidTitle) return addToast("error", "Invalid title!");
    if (invalidStartTime) return addToast("error", "Invalid start time!");
    if (invalidEndTime) return addToast("error", "Invalid end time!");

    const listUsersArr = form.contacts.map((el) => ({ [el.username]: el }));
    const listUsersObj = Object.assign({}, ...listUsersArr);

    let newId;
    try {
      newId = await addEvent(
        form.title.value,
        form.startDate.value,
        form.startTime.value,
        form.endDate.value,
        form.endTime.value,
        form.additionalInfo.value,
        form.category.value,
        form.repeats.value,
        form.privateCheckbox.value,
        listUsersObj,
        userData.username,
        userData.avatarUrl
      );
      await createNotification(listUsersObj, userData.username, newId);
      setForm(newEventInitialState);
      setSelectedUsers([{ ...userData }]);
      navigate(previousLocation);
      toggleModal();
    } catch (e) {
      addToast("error", e.message);
    }
  };

  const clearAndHideModal = () => {
    setForm(newEventInitialState);
    setSelectedUsers([{ ...userData }]);
    navigate(previousLocation);
    toggleModal();
  };

  return (
    <div className="modal modal-bottom sm:modal-middle rounded-[0px]">
      <div className="modal-box rounded-[0] lg:rounded-[0]">
        {/* Title */}
        <div className="form-control w-full max-w-xs ">
          <label className="label">
            <span className="label-text">Title</span>
          </label>
          <input
            type="text"
            placeholder="Event title"
            className="input input-bordered w-full max-w-xs"
            value={form.title.value}
            onChange={(e) => updateTitle(e.target.value)}
          />
          <label>
            {form.title.touched && !form.title.valid && (
              <span className="label-text-alt text-red-400">
                {form.title.error}
              </span>
            )}
          </label>
        </div>

        <div className="flex flex-row gap-2">
          {/* Start date */}
          <div className="form-control w-full max-w-xs">
            <label className="label">
              <span className="label-text">Start Date</span>
            </label>
            <input
              type="date"
              className="input input-bordered w-full max-w-xs"
              value={form.startDate.value}
              onChange={(e) => updateStartDate(e.target.value)}
            />
          </div>
          {/* Start time */}
          <div className="form-control w-full max-w-xs">
            <label className="label">
              <span className="label-text">Start Time</span>
            </label>
            <input
              type="time"
              placeholder="Event title"
              className="input input-bordered w-full max-w-xs"
              value={form.startTime.value}
              onChange={(e) => updateStartTime(e.target.value)}
            />
          </div>
        </div>

        <div className="flex flex-row gap-2">
          {/* End date */}
          <div className="form-control w-full max-w-xs">
            <label className="label">
              <span className="label-text">End Date</span>
            </label>
            <input
              type="date"
              placeholder="Event title"
              className="input input-bordered w-full max-w-xs"
              min={form.startDate.value}
              value={form.endDate.value}
              onChange={(e) => updateEndDate(e.target.value)}
            />
          </div>
          {/*End time */}
          <div className="form-control w-full max-w-xs">
            <label className="label">
              <span className="label-text">End Time</span>
            </label>
            <input
              type="time"
              placeholder="Event title"
              className="input input-bordered w-full max-w-xs"
              value={form.endTime.value}
              onChange={(e) => updateEndTime(e.target.value)}
            />
          </div>
        </div>

        {/* Users Combobox */}
        {form.privateCheckbox.value === "" && (
          <MyComboboxEvents
            users={users}
            selectedUsers={selectedUsers}
            setSelectedUsers={setSelectedUsers}
            searchedName={searchedName}
            setSearchedName={setSearchedName}
          />
        )}

        {/* Additional info */}
        <div className="form-control max-w-xs">
          <label className="label">
            <span className="label-text">Additional info</span>
          </label>
          <textarea
            className="textarea textarea-bordered"
            placeholder="Additional info about the event"
            value={form.additionalInfo.value}
            onChange={(e) => updateAdditionalInfo(e.target.value)}
          ></textarea>
          <label>
            {form.additionalInfo.touched && !form.additionalInfo.valid && (
              <span className="label-text-alt text-red-400">
                {form.additionalInfo.error}
              </span>
            )}
          </label>
        </div>

        {/* Checkbox */}
        {form.contacts.length === 1 && (
          <div className="form-control">
            <label className="cursor-pointer label">
              <span className="label-text">Private event</span>
              <input
                type="checkbox"
                value="checked"
                onChange={(e) => togglePrivateCheckbox(e.target.value)}
                className="checkbox checkbox-info"
              />
            </label>
          </div>
        )}

        {/* Categories */}
        <div className="dropdown dropdown-top">
          <label tabIndex={0} className="btn btn-ghost m-1 gap-2">
            <SlTag /> Categorize
          </label>
          <ul
            tabIndex={0}
            className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <div className="form-control hover:bg-red-200">
              <label className="label cursor-pointer">
                <SlTag className="text-red-500" />{" "}
                <span className="label-text">Red category</span>
                <input
                  type="radio"
                  name="categories"
                  className="radio checked:bg-red-500"
                  value="red"
                  checked={form.category.value === "red"}
                  onChange={handleCategoryChange}
                />
              </label>
            </div>
            <div className="form-control hover:bg-blue-200">
              <label className="label cursor-pointer">
                <SlTag className="text-blue-500" />{" "}
                <span className="label-text">Blue category</span>
                <input
                  type="radio"
                  name="categories"
                  className="radio checked:bg-blue-500"
                  value="blue"
                  checked={form.category.value === "blue"}
                  onChange={handleCategoryChange}
                />
              </label>
            </div>
            <div className="form-control hover:bg-yellow-200">
              <label className="label cursor-pointer">
                <SlTag className="text-yellow-500" />{" "}
                <span className="label-text">Yellow category</span>
                <input
                  type="radio"
                  name="categories"
                  className="radio checked:bg-yellow-500"
                  value="yellow"
                  checked={form.category.value === "yellow"}
                  onChange={handleCategoryChange}
                />
              </label>
            </div>
            <div className="form-control hover:bg-green-200">
              <label className="label cursor-pointer">
                <SlTag className="text-green-500" />{" "}
                <span className="label-text">Green category</span>
                <input
                  type="radio"
                  name="categories"
                  className="radio checked:bg-green-500"
                  value="green"
                  checked={form.category.value === "green"}
                  onChange={handleCategoryChange}
                />
              </label>
            </div>
            <div className="form-control hover:bg-pink-200">
              <label className="label cursor-pointer">
                <SlTag className="text-pink-500" />{" "}
                <span className="label-text">Pink category</span>
                <input
                  type="radio"
                  name="categories"
                  className="radio checked:bg-pink-500"
                  value="pink"
                  checked={form.category.value === "pink"}
                  onChange={handleCategoryChange}
                />
              </label>
            </div>
          </ul>
        </div>

        {/* Repeats */}
        <div className="dropdown dropdown-top">
          <label tabIndex={1} className="btn btn-ghost m-1 gap-2">
            <SlLoop />
            Repeats
          </label>
          <ul
            tabIndex={1}
            className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <div className="form-control hover:bg-slate-200">
              <label className="label cursor-pointer">
                <SlLoop />
                <span className="label-text">Weekly</span>
                <input
                  type="radio"
                  name="repeatingFrequency"
                  className="radio checked:bg-gray-500"
                  value="weekly"
                  checked={form.repeats.value === "weekly"}
                  onChange={handleRepeatsChange}
                />
              </label>
            </div>
            <div className="form-control hover:bg-slate-200">
              <label className="label cursor-pointer">
                <SlLoop />
                <span className="label-text">Monthly</span>
                <input
                  type="radio"
                  name="repeatingFrequency"
                  className="radio checked:bg-gray-500"
                  value="monthly"
                  checked={form.repeats.value === "monthly"}
                  onChange={handleRepeatsChange}
                />
              </label>
            </div>
            <div className="form-control hover:bg-slate-200">
              <label className="label cursor-pointer">
                <SlLoop />
                <span className="label-text">Yearly</span>
                <input
                  type="radio"
                  name="repeatingFrequency"
                  className="radio checked:bg-gray-500"
                  value="yearly"
                  checked={form.repeats.value === "yearly"}
                  onChange={handleRepeatsChange}
                />
              </label>
            </div>
          </ul>
        </div>

        <div className="flex flex-row-reverse gap-2">
          <button onClick={submitEvent}>
            <label className="btn min-w-auto w-32 h-10 bg-blue-300 p-2 rounded-xl hover:bg-blue-500 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold">
              Add event
            </label>
          </button>

          <label
            onClick={clearAndHideModal}
            className="btn min-w-auto w-32 h-10 bg-gray-500 p-2 rounded-xl hover:bg-gray-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
          >
            Cancel
          </label>
        </div>
      </div>
    </div>
  );
}
