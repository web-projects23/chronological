import { useEffect, useState, useCallback } from "react";
import format from "date-fns/format";
import {WEATHER_API_KEY} from '../../common/validation-enum';

export default function WeatherCard() {
  const [city, setCity] = useState("");
  const [description, setDescription] = useState("");
  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);
  const [id, setId] = useState("");
  const [img, setImg] = useState("");
  const [date, setDate] = useState(new Date());
  const [temp, setTemp] = useState("");

  const fetchData = useCallback(async () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        setLatitude(position.coords.latitude);
        setLongitude(position.coords.longitude);
      });
    } else {
      throw new Error("Your browser doesn't support Geolocation!");
    }

    await fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${WEATHER_API_KEY}&units=metric`
    )
      .then((res) => res.json())
      .then((data) => {
        setCity(data.name);
        setDescription(data.weather[0].description);
        setId(data.weather[0].id);
        setTemp(Math.round(data.main.temp));
        setDate(new Date(data.dt * 1000));
        check();
      });
  }, [city, latitude, longitude]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  function check() {
    if (id >= 200 && id <= 232) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/scattered-thunderstorms.svg"
      );
    } else if (id >= 300 && id <= 321) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/rainy-2.svg"
      );
    } else if (id >= 500 && id <= 531) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/rainy-3.svg"
      );
    } else if (id >= 600 && id <= 622) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/snowy-2.svg"
      );
    } else if (id === 741) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/fog.svg"
      );
    } else if (id === 761) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/dust.svg"
      );
    } else if (id >= 701 && id <= 781) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/haze.svg"
      );
    } else if (id === 800) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/clear-day.svg"
      );
    } else if (id >= 801 && id <= 804) {
      setImg(
        "https://raw.githubusercontent.com/Makin-Things/weather-icons/master/animated/cloudy.svg"
      );
    } else {
      setImg();
    }
  }

  if (city === undefined || latitude === undefined || longitude === undefined) {
    return (
      <div className="flex flex-col items-center p-8 rounded-md w-60 sm:px-12 text-white">
        <p>Loading...</p>
      </div>
    );
  } else {
    return (
      <div className="flex flex-col items-center p-8 rounded-md w-60 sm:px-12 text-white">
        <div className="text-center">
          <h2 className="text-xl font-semibold">{city}</h2>
          <p className="text-sm text-white">{format(date, "dd, MMMM")}</p>
        </div>
        <img className="pic" src={img} alt="weather"></img>
        <div className="mb-2 text-3xl font-semibold">{`${temp}°`}</div>
        <p className="text-white">{description}</p>
      </div>
    );
  }
}
