import { useContext, useEffect, useState } from "react";
import { addWeeks, endOfWeek, parseISO, startOfWeek } from "date-fns";
import CurrentWeekHeader from "../Calendar/CurrentWeekHeader";
import WeekDays from "../Calendar/WeekDays";
import WeekDaysColumns from "../Calendar/WeekDaysColumns";
import StickyHoursColumn from "../Calendar/StickyHoursColumn";
import HourRows from "../Calendar/HourRows";
import { getAllEvents } from "../../services/events.service";
import Events from "../Calendar/Events";
import { useLocation } from "react-router-dom";
import { validation } from "../../common/validation-enum";
import { AppContext } from "../../context/app.context";

export default function Week({ showModal, toggleModal, asd }) {
  const { userData } = useContext(AppContext);
  const [weekOffset, setWeekOffset] = useState(0);
  const [events, setEvents] = useState([]);
  const location = useLocation();
  const currentDate = new Date();

  useEffect(() => {
    getAllEvents(userData?.username)
      .then((events) => {
        setEvents(
          events.filter((event) => {
            return (
              parseISO(event.startDate) >= firstDayOfWeek &&
              parseISO(event.startDate) <= lastDayOfWeek
            );
          })
        );
      })
      .catch(console.error);
  }, [location, weekOffset]);

  const nextWeek = () => {
    setWeekOffset(weekOffset + 1);
  };

  const prevWeek = () => {
    setWeekOffset(weekOffset - 1);
  };

  const resetSelectedToCurrentDate = () => {
    setWeekOffset(0);
  };

  const weekInView = addWeeks(currentDate, weekOffset);
  const firstDayOfWeek = startOfWeek(weekInView, { weekStartsOn: 1 });
  const lastDayOfWeek = endOfWeek(weekInView, { weekStartsOn: 1 });
  const daysCount = validation.WEEK_DAYS_COUNT;

  return (
    <div className="md:pl-64">
      <div className="flex h-full flex-col">
        <CurrentWeekHeader
          weekInView={weekInView}
          prevWeek={prevWeek}
          nextWeek={nextWeek}
          resetSelectedToCurrentDate={resetSelectedToCurrentDate}
          lastDayOfWeek={lastDayOfWeek}
          showModal={showModal}
          toggleModal={toggleModal}
        />
        <div className="flex flex-col overflow-auto bg-white h-[80vh]">
          <WeekDays
            weekInView={weekInView}
            currentDate={currentDate}
            daysCount={daysCount}
          />
          <div className="flex">
            <StickyHoursColumn />
            <div className="grid flex-auto grid-cols-1 grid-rows-1">
              <HourRows />
              <WeekDaysColumns daysCount={daysCount} />
              <Events
                events={events}
                numDaysInView={7}
                toggleModal={toggleModal}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
