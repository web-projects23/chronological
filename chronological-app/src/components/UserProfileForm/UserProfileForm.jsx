import { useContext, useEffect, useState } from 'react';
import { AppContext } from '../../context/app.context';
import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import {
  updatePhoneNumber,
  updateName,
  updateAvatar,
  getUserById,
} from '../../services/users.service';
import { auth, storage } from '../../config/firebase-config';
import { useParams } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { updateEmail, updatePassword } from 'firebase/auth';
import { RiUserSettingsFill, RiUserLine } from 'react-icons/ri';
import waves from '../../assets/images/purple-waves-2.svg';

export default function UserProfileForm() {
  const { addToast, userData, setAppState, ...appState } =
    useContext(AppContext);
  const [userInfo, setUserInfo] = useState();
  const { uid } = useParams();

  const emailRegEx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  const phoneRegEx = /^[0-9]{10}$/g;
  const passwordRegEx =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,30}$/g;
  const firstNameRegEx = /^[A-Za-z]{1,30}$/g;
  const lastNameRegEx = /^[A-Za-z]{1,30}$/g;

  const [form, setForm] = useState({
    firstName: {
      value: '',
      touched: false,
    },
    lastName: {
      value: '',
      touched: false,
    },
    phoneNumber: {
      value: '',
      touched: false,
    },
    newEmail: {
      value: '',
      touched: false,
    },
    newPassword: {
      value: '',
      touched: false,
    },
    confNewPassword: {
      value: '',
      touched: false,
    },
  });

  const updateFirstName = (value = '') => {
    setForm({
      ...form,
      firstName: {
        value,
        touched: value === '' ? false : true,
      },
    });
  };

  const updateLastName = (value = '') => {
    setForm({
      ...form,
      lastName: {
        value,
        touched: value === '' ? false : true,
      },
    });
  };

  const updatePhoneNum = (value = '') => {
    setForm({
      ...form,
      phoneNumber: {
        value,
        touched: value === '' ? false : true,
      },
    });
  };

  const newEmailText = (value = '') => {
    setForm({
      ...form,
      newEmail: {
        value,
        touched: value === '' ? false : true,
      },
    });
  };

  const formNewPasswordText = (value = '') => {
    setForm({
      ...form,
      newPassword: {
        value,
        touched: value === '' ? false : true,
      },
    });
  };

  const formConfPasswordText = (value = '') => {
    setForm({
      ...form,
      confNewPassword: {
        value,
        touched: value === '' ? false : true,
      },
    });
  };

  useEffect(() => {
    getUserById(uid).then(setUserInfo);
  }, [uid]);

  const currUser = userData;
  const viewedUser = userInfo;

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) return addToast('error', 'Please select a file');

    const picture = storageRef(storage, `images/${userData?.username}/avatar`);

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateAvatar(userData.username, url).then(
            setAppState({
              ...appState,
              userData: {
                ...userData,
                avatarUrl: url,
              },
            })
          );
        });
      })
      .catch((e) => console.log(e.message));
  };

  const validateEmail = emailRegEx.test(form.newEmail.value);
  const validatePassword = passwordRegEx.test(form.newPassword.value);
  const validatePhoneNum = phoneRegEx.test(form.phoneNumber.value);
  const validateFirstName = firstNameRegEx.test(form.firstName.value);
  const validateLastName = lastNameRegEx.test(form.lastName.value);

  const changeName = async () => {
    if (!validateFirstName) {
      return addToast('error', 'Enter valid first name!');
    }
    if (!validateLastName) {
      return addToast('error', 'Enter valid last name!');
    }

    if (
      form.firstName.value === userData.firstName &&
      form.lastName.value === userData.lastName
    ) {
      return addToast('error', 'No changes were made!');
    }

    try {
      await updateName(
        userData.username,
        form.firstName.value,
        form.lastName.value
      );
      addToast('success', 'Information updated successfully!');

      setAppState({
        ...appState,
        userData: {
          ...userData,
          firstName: form.firstName.value,
          lastName: form.lastName.value,
        },
      });
      return;
    } catch (e) {
      addToast('error', e.message);
    }
  };

  const changeEmail = async () => {
    if (!validateEmail) return addToast('error', 'Invalid Email');

    try {
      await updateEmail(auth.currentUser, form.newEmail.value);
      addToast('success', 'Email address changed successfully!');
    } catch (error) {
      if (error.message.includes('auth/requires-recent-login'))
        return addToast('error', 'You need to re-login first');
      if (error.message.includes('auth/invalid-email'))
        return addToast('error', 'Invalid email');
      addToast('error', error.message);
    }
  };

  const changePhoneNum = async () => {
    if (!validatePhoneNum) {
      return addToast('error', 'Enter valid phone number!');
    }

    if (form.phoneNumber.value === userData.phoneNumber) {
      return addToast('error', 'No changes were made!');
    }

    try {
      await updatePhoneNumber(userData.username, form.phoneNumber.value);
      addToast('success', 'Phone number updated successfully!');

      setAppState({
        ...appState,
        userData: {
          ...userData,
          phoneNumber: form.phoneNumber.value,
        },
      });
      return;
    } catch (e) {
      addToast('error', e.message);
    }
  };

  const changePassword = async () => {
    if (!validatePassword) return addToast('error', 'Invalid Password');
    if (!validatePassword) return addToast('error', 'Invalid Password');
    if (form.newPassword.value !== form.confNewPassword.value)
      return addToast('error', "Password doesn't match");

    try {
      await updatePassword(auth.currentUser, form.newPassword.value);
      addToast('success', 'Password changed successfully!');
    } catch (error) {
      if (error.message.includes('auth/requires-recent-login'))
        return addToast('error', 'You need to re-login first');
      addToast('error', error.message);
    }
  };

  return (
    <div className="hero text-black min-h-screen bg-user-pattern md:pl-64">
      <Helmet title="User Profile" />
      <div className="hero-overlay bg-opacity-10"></div>
      <div className="flex flex-wrap justify-center items-center">
        <div className="p-3 bg-white rounded-2xl shadow-xl md:w-[46rem] w-fit">
          {/* Change Name Modal */}
          <input type="checkbox" id="profile-modal" className="modal-toggle" />
          <label htmlFor="profile-modal" className="modal cursor-pointer">
            <label
              className="modal-box relative w-fit bg-white rounded-none"
              htmlFor="profile-modal"
            >
              <div className="form-control w-full max-w-xs">
                <span className="flex justify-center text-lg label-text mb-5  text-black">
                  Customize your information
                </span>
                <span className="label-text my-1 text-black">First Name</span>
                <input
                  placeholder="Enter first name"
                  value={form.firstName.value}
                  onChange={(e) => updateFirstName(e.target.value)}
                  type="text"
                  className="input input-bordered mt-1 w-full max-w-xs bg-white"
                />
                {form.firstName.touched && !validateFirstName && (
                  <span className="label-text-alt text-red-400 mt-1.5">
                    {'Invalid name'}
                  </span>
                )}
                <span className="label-text my-1 text-black">Last Name</span>
                <input
                  placeholder="Enter last name"
                  value={form.lastName.value}
                  onChange={(e) => updateLastName(e.target.value)}
                  type="text"
                  className="input input-bordered mt-1 w-full max-w-xs bg-white"
                />
                {form.lastName.touched && !validateLastName && (
                  <span className="label-text-alt text-red-400 mt-1.5">
                    {'Invalid name'}
                  </span>
                )}
                <div
                  className="flex justify-end modal-action"
                  htmlFor="profile-modal"
                >
                  <a
                    htmlFor="profile-modal"
                    className="btn min-w-auto w-32 h-10 bg-blue-500 p-2 rounded-xl hover:bg-blue-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
                    onClick={changeName}
                  >
                    Save Changes
                  </a>
                </div>
              </div>
            </label>
          </label>

          {/* Change Password Modal */}
          <input
            type="checkbox"
            id="change-password"
            className="modal-toggle"
          />
          <label htmlFor="change-password" className="modal cursor-pointer">
            <label
              className="modal-box w-fit rounded-none"
              htmlFor="change-password"
            >
              <span className="flex justify-center text-lg label-text mb-5  text-black">
                Customize your credentials
              </span>
              <span className="label-text my-1 text-black">New password</span>
              <br />
              <input
                placeholder="Enter new password"
                onChange={(e) => formNewPasswordText(e.target.value)}
                type="password"
                className="input input-bordered mt-1 w-full max-w-xs bg-white"
              />
              <br />
              {form.newPassword.touched && !validatePassword && (
                <span className="label-text-alt text-red-400 mt-1.5">
                  {'Invalid password'}
                  <br />
                </span>
              )}
              <span className="label-text my-1 text-black">
                Repeat new password
              </span>
              <br />
              <input
                placeholder="Repeat new password"
                onChange={(e) => formConfPasswordText(e.target.value)}
                type="password"
                className="input input-bordered mt-1 w-full max-w-xs bg-white"
              />
              <br />
              {form.confNewPassword.touched && !validatePassword && (
                <span className="label-text-alt text-red-400 mt-1.5">
                  {'Invalid password'}
                  <br />
                </span>
              )}
              <div className="modal-action">
                <a
                  htmlFor="change-password"
                  className="btn min-w-auto w-32 h-10 bg-blue-500 p-2 rounded-xl hover:bg-blue-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
                  onClick={changePassword}
                >
                  Save Changes
                </a>
              </div>
            </label>
          </label>

          {/* Change Email Modal */}
          <input type="checkbox" id="change-email" className="modal-toggle" />
          <label htmlFor="change-email" className="modal cursor-pointer">
            <label
              className="modal-box w-fit rounded-none"
              htmlFor="change-email"
            >
              <span className="flex justify-center text-lg label-text mb-5  text-black">
                Customize your credentials
              </span>
              <span className="label-text my-1 text-black">
                New email address
              </span>
              <input
                placeholder="Enter new email address"
                value={form.newEmail.value}
                onChange={(e) => newEmailText(e.target.value)}
                type="text"
                className="input input-bordered mt-1 w-full max-w-xs bg-white"
              />
              <br />
              {form.newEmail.touched && !validateEmail && (
                <span className="label-text-alt text-red-400 mt-1.5">
                  {'Invalid email'}
                  <br />
                </span>
              )}
              <div
                className="flex justify-end modal-action"
                htmlFor="profile-modal"
              >
                <a
                  htmlFor="profile-modal"
                  className="btn min-w-auto w-32 h-10 bg-blue-500 p-2 rounded-xl hover:bg-blue-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
                  onClick={changeEmail}
                >
                  Save Changes
                </a>
              </div>
            </label>
          </label>

          {/* Change Phone Modal */}
          <input type="checkbox" id="phone-modal" className="modal-toggle" />
          <label htmlFor="phone-modal" className="modal cursor-pointer">
            <label
              className="modal-box w-fit rounded-none"
              htmlFor="phone-modal"
            >
              <span className="flex justify-center text-lg label-text mb-5  text-black">
                Customize your information
              </span>
              <span className="label-text my-1 text-black">
                New phone number
              </span>
              <input
                placeholder="Enter new phone number"
                value={form.phoneNumber.value}
                onChange={(e) => updatePhoneNum(e.target.value)}
                type="text"
                className="input input-bordered mt-1 w-full max-w-xs bg-white"
              />
              <br />
              {form.phoneNumber.touched && !validatePhoneNum && (
                <span className="label-text-alt text-red-400 mt-1.5">
                  {'Invalid phone number'}
                  <br />
                </span>
              )}
              <div
                className="flex justify-end modal-action"
                htmlFor="profile-modal"
              >
                <a
                  htmlFor="profile-modal"
                  className="btn min-w-auto w-32 h-10 bg-blue-500 p-2 rounded-xl hover:bg-blue-700 transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold"
                  onClick={changePhoneNum}
                >
                  Save Changes
                </a>
              </div>
            </label>
          </label>

          <div className="hero w-full h-[250px]">
            <img
              src={waves}
              className="w-full h-full rounded-tl-lg rounded-tr-lg"
            />
          </div>
          <div className="flex flex-col items-center -mt-20">
            {currUser?.username === viewedUser?.username &&
              currUser?.username && (
                <div className="flex avatar justify-center pb-5">
                  <div className="w-40 border-4 border-white rounded-full">
                    <img src={currUser.avatarUrl} alt="avatar" />
                  </div>
                </div>
              )}
            {currUser?.username !== viewedUser?.username &&
              currUser?.username && (
                <div className="flex avatar justify-center pb-5">
                  <div className="w-40 border-4 border-white rounded-full">
                    <img src={viewedUser?.avatarUrl} alt="avatar" />
                  </div>
                </div>
              )}
            <div className="flex items-center space-x-2 mt-2">
              <p className="text-2xl">
                {currUser?.username === viewedUser?.username &&
                  currUser?.username}
                {currUser?.username !== viewedUser?.username &&
                  viewedUser?.username}
              </p>
              <span className="bg-blue-500 rounded-full p-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="text-gray-100 h-2.5 w-2.5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="4"
                    d="M5 13l4 4L19 7"
                  ></path>
                </svg>
              </span>
            </div>
            {currUser?.username === viewedUser?.username && (
              <p className="text-sm text-gray-500">@{currUser?.role}</p>
            )}
            {currUser?.username !== viewedUser?.username && (
              <p className="text-sm text-gray-500">@{viewedUser?.role}</p>
            )}
          </div>
          <div className="flex justify-center mt-2">
            {currUser?.username === viewedUser?.username && (
              <label
                htmlFor="change-avatar"
                className="flex items-center bg-blue-500 hover:bg-blue-600 text-gray-100 px-4 py-2 rounded text-sm space-x-2 transition duration-100 cursor-pointer"
              >
                <span>Change Avatar</span>
              </label>
            )}
            <input
              type="checkbox"
              id="change-avatar"
              className="modal-toggle"
            />
            <div className="modal">
              <div className="modal-box rounded-none">
                <button>
                  <label
                    htmlFor="change-avatar"
                    className="btn btn-sm btn-ghost btn-circle absolute right-2 top-2"
                  >
                    ✕
                  </label>
                </button>
                {userData?.avatarUrl && (
                  <div className="avatar flex justify-center">
                    <div className="w-64 rounded-full">
                      <img src={userData.avatarUrl} />
                    </div>
                  </div>
                )}
                <form onSubmit={uploadPicture}>
                  <div className="flex justify-between items-center m-3 p-1">
                    <input
                      type="file"
                      className="text-sm text-grey-500
                      file:mr-3 file:py-3 file:px-10
                      file:rounded-2xl file:border-0
                      file:text-md file:font-semibold  file:text-white
                      file:bg-gradient-to-r file:from-blue-400 file:to-indigo-700
                      hover:file:cursor-pointer hover:file:opacity-80"
                    />
                    <button
                      className="btn rounded-2xl bg-indigo-700 hover:bg-indigo-600"
                      type="submit"
                    >
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div className="flex-1 bg-white rounded-lg shadow-md p-6">
            <div className="flex justify-between items-center">
              <h4 className="flex text-xl text-gray-900 font-bold">
                <RiUserLine className="mr-2 w-6 h-6" />
                User details
              </h4>
              {/* Dropdown button */}
              {currUser?.username === viewedUser?.username && (
                <div className="dropdown dropdown-end">
                  <label tabIndex={0} className="btn btn-ghost btn-circle">
                    <RiUserSettingsFill className="w-6 h-6" />
                  </label>
                  <ul
                    tabIndex={0}
                    className="dropdown-content menu text-sm p-1 shadow bg-white rounded-md w-52"
                  >
                    <li>
                      <label htmlFor="profile-modal">Edit full name</label>
                    </li>
                    <li>
                      <label htmlFor="phone-modal">Edit phone number</label>
                    </li>
                    <li>
                      <label htmlFor="change-email">Change Email</label>
                    </li>
                    <li>
                      <label htmlFor="change-password">Change Password</label>
                    </li>
                  </ul>
                </div>
              )}
            </div>

            <ul className="mt-2 text-gray-700">
              {currUser?.username === viewedUser?.username && (
                <li className="flex border-y py-2">
                  <span className="font-bold w-24">Full name:</span>
                  <span className="text-gray-700">{`${currUser?.firstName} ${currUser?.lastName}`}</span>
                </li>
              )}
              {currUser?.username !== viewedUser?.username && (
                <li className="flex border-y py-2">
                  <span className="font-bold w-24">Full name:</span>
                  <span className="text-gray-700">{`${viewedUser?.firstName} ${viewedUser?.lastName}`}</span>
                </li>
              )}
              {currUser?.username === viewedUser?.username && (
                <li className="flex border-b py-2">
                  <span className="font-bold w-24">Phone:</span>
                  <span className="text-gray-700">{currUser?.phoneNumber}</span>
                </li>
              )}
              {currUser?.username !== viewedUser?.username && (
                <li className="flex border-b py-2">
                  <span className="font-bold w-24">Phone:</span>
                  <span className="text-gray-700">
                    {viewedUser?.phoneNumber}
                  </span>
                </li>
              )}
              {currUser?.username === viewedUser?.username && (
                <li className="flex border-b py-2">
                  <span className="font-bold w-24">Email:</span>
                  <span className="text-gray-700">{currUser?.email}</span>
                </li>
              )}
              {currUser?.username !== viewedUser?.username && (
                <li className="flex border-b py-2">
                  <span className="font-bold w-24">Email:</span>
                  <span className="text-gray-700">{viewedUser?.email}</span>
                </li>
              )}
              {currUser?.username === viewedUser?.username && (
                <li className="flex border-b py-2">
                  <span className="font-bold w-24">Joined:</span>
                  <span className="text-gray-700">
                    {new Date(currUser?.registeredOn).toDateString()}
                  </span>
                </li>
              )}
              {currUser?.username !== viewedUser?.username && (
                <li className="flex border-b py-2">
                  <span className="font-bold w-24">Joined:</span>
                  <span className="text-gray-700">
                    {new Date(viewedUser?.registeredOn).toDateString()}
                  </span>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
