import { addDays, startOfWeek } from "date-fns";
import format from "date-fns/format";
import useWindowSize from "../../hooks/windowSize";

export default function CurrentDay({
  dayInView,
  currentDate,
  selectedDate,
  setSelectedDate,
}) {
  const firstDayOfWeek = startOfWeek(dayInView, { weekStartsOn: 1 });
  const windowSize = useWindowSize();

  const isDaySelected = (day) => {
    return (
      selectedDate.getDate() === day.getDate() &&
      selectedDate.getMonth() === day.getMonth()
    );
  };

  const isDayCurrentDate = (day) => {
    return (
      currentDate.getDate() === day.getDate() &&
      currentDate.getMonth() === day.getMonth()
    );
  };

  return (
    <div className="sticky top-0 z-30 flex-none bg-white shadow ring-1 ring-black ring-opacity-5">
      <div className="grid text-sm leading-6 text-gray-500 grid-cols-7">
        <div className="col-end-1 w-14" />
        {[...Array(7).keys()].map((index) => {
          const day = addDays(firstDayOfWeek, index);
          return (
            <button
              type="button"
              key={day}
              className="flex flex-col sm:flex-row items-center justify-center py-3"
              onClick={() => setSelectedDate(day)}
            >
              {windowSize.width > 600
                ? format(day, "EEE")
                : format(day, "EEEEE")}

              <span
                className={`items-center justify-center font-semibold ${
                  isDayCurrentDate(day) || isDaySelected(day)
                    ? "ml-1.5 w-5 h-5 flex sm:w-8 sm:h-8 rounded-full text-white"
                    : "text-gray-700 ml-1.5"
                } 
                    ${isDayCurrentDate(day) && " bg-sky-500 "}
                    ${
                      isDaySelected(day) && " bg-violet-500"
                    }                      
                }`}
              >
                {format(day, "dd")}
              </span>
            </button>
          );
        })}
      </div>
    </div>
  );
}
