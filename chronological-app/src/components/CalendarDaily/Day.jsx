import { useContext, useEffect, useState } from "react";
import CurrentDay from "./CurrentDay";
import CurrentDayHeader from "./CurrentDayHeader";
import StickyHoursColumn from "../Calendar/StickyHoursColumn";
import HourRows from "../Calendar/HourRows";
import { addDays, format } from "date-fns/esm";
import Events from "../Calendar/Events";
import { useLocation } from "react-router-dom";
import { getAllEvents } from "../../services/events.service";
import { differenceInDays, startOfDay } from "date-fns";
import { AppContext } from "../../context/app.context";

export default function Day({ toggleModal }) {
  const { userData } = useContext(AppContext);
  const dateToday = new Date();
  const [dayOffset, setDayOffset] = useState(0);
  const [events, setEvents] = useState([]);
  const location = useLocation();

  useEffect(() => {
    getAllEvents(userData?.username)
      .then((events) => {
        setEvents(
          events.filter((event) => {
            return event.startDate === format(selectedDate(), "yyyy-MM-dd");
          })
        );
      })
      .catch(console.error);
  }, [location, dayOffset]);

  useEffect(() => {
    if (location?.state?.date) {
      const differenceInDaysBetween = differenceInDays(
        location.state.date,
        startOfDay(dateToday)
      );

      setDayOffset(differenceInDaysBetween);
    }
  }, [location]);

  const nextDay = () => {
    setDayOffset(dayOffset + 1);
  };
  const prevDay = () => {
    setDayOffset(dayOffset - 1);
  };

  const selectedDate = () => {
    return startOfDay(addDays(dateToday, dayOffset));
  };
  const setSelectedDate = (day) => {
    setDayOffset(day.getDate() - dateToday.getDate());
  };

  const dayInView = addDays(dateToday, dayOffset);

  const resetSelectedToCurrentDate = () => {
    setDayOffset(0);
  };

  return (
    <div className="md:pl-64">
      <div className="flex h-full flex-col">
        <CurrentDayHeader
          dayInView={dayInView}
          prevDay={prevDay}
          nextDay={nextDay}
          resetSelectedToCurrentDate={resetSelectedToCurrentDate}
          toggleModal={toggleModal}
          selectedDate={selectedDate()}
        />
      </div>
      <div className="flex flex-col overflow-auto bg-white h-[80vh]">
        <div className="flex max-w-full flex-none flex-col sm:max-w-none md:max-w-full">
          <CurrentDay
            dayInView={dayInView}
            currentDate={dateToday}
            selectedDate={selectedDate()}
            setSelectedDate={setSelectedDate}
          />
          <div className="flex flex-auto">
            <StickyHoursColumn />
            <div className="grid flex-auto grid-cols-1 grid-rows-1">
              <HourRows />
              <Events
                events={events}
                numDaysInView={1}
                toggleModal={toggleModal}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
