import { Fragment } from 'react';
import { Combobox } from '@headlessui/react';
import { AiOutlineUsergroupAdd } from 'react-icons/ai';

export default function MyComboboxEvents({
  users,
  selectedUsers,
  setSelectedUsers,
  searchedName,
  setSearchedName,
}) {
  const getContactsDiff = (allUsers, selectedContacts) => {
    return allUsers.filter((newContact) => {
      return !selectedContacts.some((currContact) => {
        return newContact.uid === currContact.uid;
      });
    });
  };

  const contactsDiff = getContactsDiff(users, selectedUsers);

  const filteredUsers =
    searchedName === ''
      ? contactsDiff
      : contactsDiff.filter((user) => {
          return (
            user.firstName.toLowerCase().includes(searchedName.toLowerCase()) ||
            user.lastName.toLowerCase().includes(searchedName.toLowerCase()) ||
            user.phoneNumber.includes(searchedName)
          );
        });

  return (
    <Combobox value={selectedUsers} onChange={setSelectedUsers} multiple>
      <div className="relative mt-1">
        <span className="label-text">Add user</span>
        <Combobox.Input
          className="w-full input input-bordered h-10 my-1 p-3"
          placeholder="Enter name or phone number"
          displayValue={''}
          onChange={(event) => setSearchedName(event.target.value)}
        />
        <Combobox.Options>
          {filteredUsers.map((user) => (
            <Combobox.Option key={user.uid} value={user} as={Fragment}>
              {({ active, selected }) => (
                <li
                  className={`border-x-2 border-b-2 ${
                    active ? 'bg-blue-300 text-white' : 'bg-white text-black'
                  }`}
                >
                  {selected}
                  <div className="flex items-center p-1 space-x-4">
                    <div className="flex-shrink-0">
                      <div className="flex avatar justify-center">
                        <div className="w-8 mr-3 rounded-full">
                          <img src={user.avatarUrl} alt="avatar" />
                        </div>
                      </div>
                    </div>
                    <div className="flex-1 min-w-0">
                      <p className="text-sm font-medium text-gray-900 truncate">
                        {`${user.firstName} ${user.lastName}`}
                      </p>
                    </div>
                  </div>
                </li>
              )}
            </Combobox.Option>
          ))}
        </Combobox.Options>
        {selectedUsers.length > 0 && (
          <div className="my-2">
            {selectedUsers.map((user, i) => (
              <div
                key={i}
                className="flex flex-row items-center justify-between p-1 bg-white border-b-2 border-gray-100"
              >
                <div className="flex flex-row items-center">
                  <div className="flex avatar justify-center">
                    <div className="w-10 mr-3 rounded-full">
                      <img src={user.avatarUrl} alt="avatar" />
                    </div>
                  </div>
                  <div className="flex flex-col">
                    <p>{`${user.firstName} ${user.lastName}`}</p>
                    <p className="text-sm text-gray-400">{user.username}</p>
                  </div>
                </div>
                <h1 className="text-blue-300 text-3xl">
                  <AiOutlineUsergroupAdd />
                </h1>
              </div>
            ))}
          </div>
        )}
      </div>
    </Combobox>
  );
}
