import {
  RiCalendarLine,
  RiCalendarEventLine,
  RiContactsBookLine,
  RiInformationLine,
  RiLogoutBoxRLine,
} from "react-icons/ri";
import { TbUsers } from "react-icons/tb";
import WeatherCard from "../../components/WeatherCard/WeatherCard";
import Logo from "../../assets/images/Logo.png";
import { AppContext } from "../../context/app.context";
import { logoutUser } from "../../services/auth.service";
import { useContext, useState } from "react";
import { Link, useLocation } from "react-router-dom";

const navigation = [
  { name: "Calendar", navigate: "/calendar/month", icon: RiCalendarLine },
  { name: "My Events", navigate: "/my-events", icon: RiCalendarEventLine },
  { name: "Contact list", navigate: "/my-contacts", icon: RiContactsBookLine },
  { name: "Users", navigate: "/users", icon: TbUsers },
  { name: "About us", navigate: "/about", icon: RiInformationLine },
  { name: "Logout", navigate: "/login", icon: RiLogoutBoxRLine },
];

export default function SidebarNav() {
  const { setAppState, ...appState } = useContext(AppContext);
  const location = useLocation();
  const currentPath = navigation.findIndex(
    (route) => route.navigate === location.pathname
  );
  const [active, setActive] = useState(currentPath > -1 ? currentPath : 0);

  const logout = async () => {
    await logoutUser();

    setAppState({
      ...appState,
      user: null,
      userData: null,
    });
  };

  return (
    <div className="hidden md:flex md:w-64 md:flex-col md:fixed md:inset-y-0">
      <div className="flex flex-col flex-grow pt-6 bg-gradient-to-b from-indigo-500 to-blue-400 overflow-y-auto">
        <div className="flex items-center flex-shrink-0 px-4">
          <img className="h-14 w-auto" src={Logo} alt="ChronologiCal" />
          <div className="px-5 text-xl text-white">ChronologiCal</div>
        </div>
        <div className="mt-5 flex flex-col">
          <nav className="flex-1 px-2 pb-4 space-y-1">
            {navigation.map((item, index) => (
              <Link
                key={index}
                to={item.navigate}
                onClick={() => {
                  setActive(index);
                  item.name === "Logout" ? { logout } : null;
                }}
                className={
                  active === index
                    ? "bg-indigo-800 text-white group flex items-center px-2 py-2 text-sm font-medium rounded-md"
                    : "text-indigo-100 hover:bg-indigo-600 group flex items-center px-2 py-2 text-sm font-medium rounded-md"
                }
              >
                <item.icon
                  className="mr-3 flex-shrink-0 h-7 w-7 text-white"
                  aria-hidden="true"
                />
                {item.name}
              </Link>
            ))}
          </nav>
        </div>
        <div className="flex-1 flex flex-col"></div>
        <WeatherCard />
      </div>
    </div>
  );
}
