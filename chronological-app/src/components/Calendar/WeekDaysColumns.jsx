export default function WeekDaysColumns({ daysCount }) {
  const numGridCols =
    daysCount === 5
      ? "grid-cols-5 sm:grid-cols-5"
      : "grid-cols-7 sm:grid-cols-7";

  return (
    <div
      className={
        "col-start-1 col-end-2 row-start-1 hidden grid-rows-1 divide-x divide-gray-100 sm:grid " +
        numGridCols
      }
    >
      {[...Array(daysCount).keys()].map((index) => {
        return (
          <div key={index} className={`col-start-${index + 1} row-span-full`} />
        );
      })}
    </div>
  );
}
