import { format, startOfWeek, addDays } from "date-fns";
export default function WeekDays({ weekInView, currentDate, daysCount }) {
  const firstDayOfWeek = startOfWeek(weekInView, { weekStartsOn: 1 });

  const isDayCurrentDate = (day) => {
    return (
      currentDate.getDate() === day.getDate() &&
      currentDate.getMonth() === day.getMonth()
    );
  };

  const numGridCols = daysCount === 5 ? "grid-cols-5" : "grid-cols-7";

  return (
    <div className="sticky top-0 z-30 flex-none bg-white shadow ring-1 ring-black ring-opacity-5">
      {/* Week days when small (sm) screen */}
      <div
        className={
          `grid text-sm leading-6 text-gray-500 sm:hidden ` + numGridCols
        }
      >
        {[...Array(daysCount).keys()].map((index) => {
          const day = addDays(firstDayOfWeek, index);
          return (
            <button
              type="button"
              key={day}
              className="flex flex-col items-center pt-2 pb-3"
            >
              {format(day, "EEEEE")}
              <span
                className={`mt-1 flex h-8 w-8 items-center justify-center font-semibold ${
                  isDayCurrentDate(day)
                    ? "rounded-full bg-sky-600 text-white"
                    : "text-gray-700"
                }`}
              >
                {format(day, "dd")}
              </span>
            </button>
          );
        })}
      </div>

      {/* Week days when larger than small (sm) screen */}
      <div
        className={
          "hidden divide-x divide-gray-100 border-r border-gray-100 text-sm leading-6 text-gray-500 sm:grid h-14 " +
          numGridCols
        }
      >
        <div className="col-end-1 w-14" />
        {[...Array(daysCount).keys()].map((index) => {
          const day = addDays(firstDayOfWeek, index);
          return (
            <div key={day} className="flex items-center justify-center py-3">
              <span
                className={isDayCurrentDate(day) ? "flex items-baseline" : ""}
              >
                {format(day, "EEE") + " "}
                <span
                  className={`items-center justify-center font-semibold ${
                    isDayCurrentDate(day)
                      ? "ml-1.5 flex w-8 h-8 rounded-full bg-sky-500 text-white"
                      : "text-gray-700"
                  }`}
                >
                  {format(day, "dd")}
                </span>
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
}
