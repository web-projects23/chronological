import { useNavigate, useLocation } from "react-router-dom";
import useWindowSize from "../../hooks/windowSize";

export default function CalendarViewsNav() {
  const navigate = useNavigate();
  const location = useLocation();
  const currentView = location.pathname.substring(
    location.pathname.indexOf("/", 1) + 1
  );
  const windowSize = useWindowSize();

  const colorClasses = (view) => {
    if (view === currentView) {
      return "bg-gray-500 hover:bg-black";
    } else if (view === "month") {
      return "bg-indigo-500 hover:bg-indigo-700";
    } else if (view === "week") {
      return "bg-violet-500 hover:bg-violet-700";
    } else if (view === "work-week") {
      return "bg-blue-500 hover:bg-blue-700";
    } else if (view === "day") {
      return "bg-sky-500 hover:bg-sky-700";
    }
  };

  return (
    <div>
      <div className="flex p-2 w-full justify-center md:pl-64 flex-row flex-nowrap my-5">
        <button
          className={
            "min-w-auto w-32 h-10 p-2 rounded-l-xl transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold " +
            colorClasses("month")
          }
          onClick={() => {
            navigate("month");
          }}
        >
          Month
        </button>
        <button
          className={
            "min-w-auto w-32 h-10 rounded-none transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold " +
            colorClasses("week")
          }
          onClick={() => {
            navigate("week");
          }}
        >
          Week
        </button>
        <button
          className={
            "min-w-auto w-32 h-10 p-2 rounded-none transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold " +
            colorClasses("work-week")
          }
          onClick={() => {
            navigate("work-week");
          }}
        >
          {windowSize.width > 600 ? "Work week" : "WW"}
        </button>
        <button
          className={
            "min-w-auto w-32 h-10 p-2 rounded-r-xl transition-colors duration-50 hover:animate-pulse ease-out text-white font-semibold " +
            colorClasses("day")
          }
          onClick={() => {
            navigate("day");
          }}
        >
          Day
        </button>
      </div>
    </div>
  );
}
