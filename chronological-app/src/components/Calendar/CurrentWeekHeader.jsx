import DateRange from "./DateRange";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import useWindowSize from "../../hooks/windowSize";
import { format } from "date-fns";

export default function CurrentWeekHeader({
  weekInView,
  prevWeek,
  nextWeek,
  resetSelectedToCurrentDate,
  lastDayOfWeek,
  toggleModal,
}) {
  const navigate = useNavigate();
  const windowSize = useWindowSize();
  return (
    <header className="relative z-1 flex flex-none items-center justify-between border-b border-gray-200 py-2 px-6">
      <label
        onClick={() => {
          toggleModal();
          navigate("add-event");
        }}
        className="btn glass text-white text-sm bg-[#6267f1] hover:bg-violet-700 flex mr-3 text-center rounded-xl font-semibold sm:mr-6 sm:text-md"
      >
        Create event
      </label>
      {windowSize.width > 600 ? (
        <DateRange
          weekInView={weekInView}
          lastDayOfWeek={lastDayOfWeek}
        ></DateRange>
      ) : (
        <time className="mr-3">{format(weekInView, "MMM yy")}</time>
      )}
      <div className="flex items-center">
        <div className="flex items-center rounded-md shadow-sm md:items-stretch float-right">
          <button
            type="button"
            className="flex items-center justify-center rounded-l-md border border-r-0 border-gray-300 bg-white py-2 pl-3 pr-4 text-gray-700 focus:relative md:w-9 md:px-2"
            onClick={prevWeek}
          >
            <AiOutlineArrowLeft
              className="h-5 w-5 text-gray-300"
              aria-hidden="true"
            />
          </button>
          {windowSize.width > 600 && (
            <button
              type="button"
              className="border-t border-b border-gray-300 bg-white text-sm font-extrabold text-gray-500 focus:relative h-full flex items-center justify-center py-2"
              onClick={resetSelectedToCurrentDate}
            >
              Today
            </button>
          )}
          <button
            type="button"
            className="flex items-center justify-center rounded-r-md border border-l-0 border-gray-300 bg-white py-2 pl-4 pr-3 text-gray-700 focus:relative md:w-9 md:px-2"
            onClick={nextWeek}
          >
            <AiOutlineArrowRight
              className="h-5 w-5 text-gray-300"
              aria-hidden="true"
            />
          </button>
        </div>
      </div>
    </header>
  );
}
