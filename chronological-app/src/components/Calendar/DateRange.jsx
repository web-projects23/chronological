import { format, startOfWeek } from "date-fns";

export default function DateRange({ weekInView, lastDayOfWeek }) {
  const firstDayOfWeek = startOfWeek(weekInView, { weekStartsOn: 1 });

  const getDateRangeString = () => {
    const firstMonth = format(firstDayOfWeek, "MMMM");
    const lastMonth = format(lastDayOfWeek, "MMMM");
    const firstDate = firstDayOfWeek.getDate();
    const lastDate = lastDayOfWeek.getDate();
    const firstYear = firstDayOfWeek.getFullYear();
    const lastYear = lastDayOfWeek.getFullYear();

    if (firstMonth === lastMonth) {
      return `${firstMonth} ${firstDate}-${lastDate}, ${lastYear}`;
    } else {
      if (firstYear === lastYear) {
        return `${firstMonth} ${firstDate} - ${lastMonth} ${lastDate}, ${lastYear}`;
      } else {
        return `${firstMonth} ${firstDate}, ${firstYear} - ${lastMonth} ${lastDate}, ${lastYear}`;
      }
    }
  };

  return (
    <h1 className="text-lg font-semibold text-gray-900">
      <time>{getDateRangeString()}</time>
    </h1>
  );
}
