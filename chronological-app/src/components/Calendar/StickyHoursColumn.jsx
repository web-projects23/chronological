export default function StickyHoursColumn() {
  return (
    <div className="sticky left-0 z-10 w-14 flex-none bg-white ring-1 ring-gray-100" />
  );
}
