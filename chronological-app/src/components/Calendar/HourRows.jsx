import { addMinutes, format } from "date-fns";

export default function HourRows() {
  let firstTimeSlot = new Date();
  firstTimeSlot.setHours(23, 30, 0, 0);

  const getNextTimeRow = () => {
    firstTimeSlot = addMinutes(firstTimeSlot, 30);
    return format(firstTimeSlot, "HH:mm");
  };

  return (
    <div
      className="col-start-1 col-end-2 row-start-1 grid divide-y divide-gray-100"
      style={{ gridTemplateRows: "repeat(48, minmax(1.75rem, 1fr))" }}
    >
      <div className="row-end-1 h-7"></div>
      {[...Array(48).keys()].map((hour) => (
        <div key={hour}>
          <div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
            {getNextTimeRow()}
          </div>
        </div>
      ))}
    </div>
  );
}
