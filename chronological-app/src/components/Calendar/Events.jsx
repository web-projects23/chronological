import { addDays, format, parseISO } from "date-fns";
import Event from "./Event";

export default function Events({ events, numDaysInView }) {
  const calculateRowSpan = (event) => {
    const startTimeNum = Number(event.startTime.split(":").join(""));
    const endTimeNum = Number(event.endTime.split(":").join(""));
    const durationMinutes = endTimeNum - startTimeNum;

    return Math.ceil(durationMinutes / 5);
  };

  const numColumns = () => {
    switch (numDaysInView) {
      case 5:
        return "sm:grid-cols-5";
      case 7:
        return "sm:grid-cols-7";
    }
  };

  const calculateNumDivs = () => {
    return (
      2023 -
      events.reduce(
        (accumulator, event) => accumulator + calculateRowSpan(event),
        0
      )
    );
  };

  return (
    <ol
      className={
        "col-start-1 col-end-2 row-start-1 grid grid-cols-1 " + numColumns()
      }
      style={{ gridTemplateRows: "1.75rem repeat(288, minmax(0, 1fr)) auto" }}
    >
      {[...Array(calculateNumDivs()).keys()].map((index) => (
        <div key={index}></div>
      ))}
      {events.map((event) => {
        const eventStartDate = parseISO(event.startDate);
        const eventEndDate = parseISO(event.endDate);

        if (eventStartDate < eventEndDate) {
          let splitEvents = [];
          const eventNumDays =
            eventEndDate.getDate() - eventStartDate.getDate() + 1;
          for (let i = 0; i < eventNumDays; i++) {
            splitEvents.push({
              ...event,
              startDate: format(addDays(eventStartDate, i), "yyyy-MM-dd"),
              endDate: format(addDays(eventStartDate, i), "yyyy-MM-dd"),
              startTime: i === 0 ? event.startTime : "00:01",
              endTime: i === eventNumDays - 1 ? event.endTime : "23:59",
            });
          }

          const isDayView = numDaysInView === 1;

          if (isDayView) {
            return (
              <Event
                key={`${event.id}`}
                event={splitEvents[0]}
                numDaysInView={numDaysInView}
              />
            );
          }
          return splitEvents.map((splitEvent, index) => (
            <Event
              key={`${event.id}-${index}`}
              event={splitEvent}
              numDaysInView={numDaysInView}
            />
          ));
        } else {
          return (
            <Event key={event.id} event={event} numDaysInView={numDaysInView} />
          );
        }
      })}
    </ol>
  );
}
