import { format, hoursToMinutes, parseISO } from "date-fns";
import { useNavigate } from "react-router-dom";

export default function Event({ event, numDaysInView }) {
  const navigate = useNavigate();
  const numEmptyRowsOnTop = 1;
  const numMinutesInOneGridRow = 5;

  const calculateGridRowOfEvent = (event) => {
    const hours = Number(event.startTime.split(":")[0]);
    const minutes = Number(event.startTime.split(":")[1]);

    const eventDurationInMinutes = hoursToMinutes(hours) + minutes;

    return Math.ceil(
      eventDurationInMinutes / numMinutesInOneGridRow + numEmptyRowsOnTop
    );
  };

  const calculateRowSpanOfEvent = (event) => {
    const [startTimeHour, startTimeMinutes] = event.startTime.split(":");
    const [endTimeHour, endTimeMinutes] = event.endTime.split(":");

    const numMinutesInOneHour = 60;
    const startTimeNum =
      Number(startTimeHour) * numMinutesInOneHour + Number(startTimeMinutes);
    const endTimeNum =
      Number(endTimeHour) * numMinutesInOneHour + Number(endTimeMinutes);

    const durationMinutes = endTimeNum - startTimeNum;

    return Math.ceil(durationMinutes / numMinutesInOneGridRow);
  };

  const colorClasses = (event) => {
    switch (event.category) {
      case "pink":
        return "bg-pink-50 hover:bg-pink-100";
      case "purple":
        return "bg-purple-50 hover:bg-purple-100";
      case "blue":
        return "bg-blue-50 hover:bg-blue-100";
      case "green":
        return "bg-green-50 hover:bg-green-100";
      case "orange":
        return "bg-orange-50 hover:bg-orange-100";
      case "yellow":
        return "bg-yellow-50 hover:bg-yellow-100";
      case "red":
        return "bg-red-50 hover:bg-red-100";
    }
  };
  return (
    <li
      className="relative flex sm:col-start-1"
      style={{
        gridRow: `${calculateGridRowOfEvent(
          event
        )} / span ${calculateRowSpanOfEvent(event)}`,
        gridColumnStart:
          numDaysInView > 1 &&
          format(parseISO(`${event.startDate}T${event.startTime}`), "i"),
      }}
      onClick={() => navigate(event.id)}
    >
      <label
        htmlFor="my-modal-3"
        className={
          "cursor-pointer group absolute inset-0 flex flex-col overflow-y-auto p-2 text-xs leading-5 " +
          colorClasses(event)
        }
      >
        <p className={`order-1 font-semibold text-${event.category}-700`}>
          {event.title}
        </p>
        <p
          className={`text-${event.category}-500 group-hover:text-${event.category}-700`}
        >
          <time
            dateTime={parseISO(
              `${event.startDate}T${event.startTime}`
            ).toISOString()}
          >
            {event.startTime}
          </time>
        </p>
      </label>
    </li>
  );
}
