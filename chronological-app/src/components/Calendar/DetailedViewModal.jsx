import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getEventById } from "../../services/events.service";
import { AiOutlineCalendar } from "react-icons/ai";
import { IoMdTime } from "react-icons/io";
import { FcLock, FcUnlock } from "react-icons/fc";
import { RiUser3Fill } from "react-icons/ri";

export default function DetailedViewModal() {
  const { uid } = useParams();
  const [event, setEvent] = useState([]);
  const [participants, setParticipants] = useState([]);

  useEffect(() => {
    getEventById(uid).then(setEvent).catch(console.error);
  }, [uid]);

  useEffect(() => {
    setParticipants(event?.contacts);
  }, [event]);

  const colorClasses = (event) => {
    switch (event?.category) {
      case "pink":
        return "text-pink-700";
      case "purple":
        return "text-purple-700";
      case "blue":
        return "text-blue-700";
      case "green":
        return "text-green-700";
      case "orange":
        return "text-orange-700";
      case "yellow":
        return "text-yellow-700";
      case "red":
        return "text-red-700";
    }
  };

  return (
    <div>
      <input type="checkbox" id="my-modal-3" className="modal-toggle" />
      <div className="modal">
        <div className="modal-box relative">
          <label
            htmlFor="my-modal-3"
            className="btn btn-sm  absolute right-2 bottom-2"
          >
            Close
          </label>

          <div className="mt-3">
            {event?.privateCheckbox === "checked" ? (
              <p className="text-sm text-gray-600 flex items-center mb-2">
                <FcLock className="text-md mr-1" />
                Private event
              </p>
            ) : (
              <p className="text-sm text-gray-600 flex items-center mb-2">
                <FcUnlock className="text-md mr-1" />
                Public event
              </p>
            )}
            <div
              className={
                "font-extrabold text-xl mb-2 text-center " + colorClasses(event)
              }
            >
              {event?.title}
            </div>
            <div className="flex mb-2 justify-center">
              <span className="flex items-center justify-center mr-2">
                <AiOutlineCalendar className="mr-1" />
                {event?.startDate}
              </span>
              <span className="flex items-center justify-center">
                <IoMdTime className="mr-1" />
                {event?.startTime}
              </span>
            </div>

            <h3 className="text-md text-gray-500">Details:</h3>
            <p className="text-gray-700 text-base mb-2">
              {event?.additionalInfo}
            </p>
            <h3 className="text-md font-extrabold text-gray-500 mb-1">
              Participants:
            </h3>
            <ol className="text-gray-700 text-base mb-2">
              {participants &&
                Object.keys(participants).map((p, i) => (
                  <div key={i} className="flex leading-5 mb-1">
                    <RiUser3Fill className="mr-1" />
                    <div className="text-md text-gray-700">{p}</div>
                  </div>
                ))}
            </ol>
          </div>
        </div>
      </div>
    </div>
  );
}
