import { BsArrowRight } from "react-icons/bs";
import { useNavigate } from "react-router-dom";

export default function PublicHero() {
  const navigate = useNavigate();

  return (
    <div className="hero h-full bg-[url('/src/assets/images/layer-waves.svg')]">
      <div className=" px-4 mx-auto max-w-screen-xl text-center lg:py-16 lg:px-12">
        <h2 className="mb-4 text-4xl font-extrabold tracking-tight text-gray-900 md:text-5xl lg:text-6xl">
          Manage all your tasks with
        </h2>
        <h2 className=" mb-3 p-2 text-4xl font-extrabold tracking-tight md:text-5xl lg:text-6xl bg-gradient-to-r from-blue-600 via-pink-500 to-indigo-400 inline-block text-transparent bg-clip-text">
          ChronologiCal
        </h2>
        <p className="mb-10 text-lg font-normal text-gray-700 lg:text-xl sm:px-16 xl:px-48">
          Here in ChronologiCal we effectively manage your time by helping you
          with your week planning and scheduling, because we know that there are
          only as many days in the year as you make use of.
        </p>
        <button
          className="btn glass btn-wide text-gray-900 hover:bg-pink-600"
          onClick={() => {
            navigate("/register");
          }}
        >
          Get started
          <BsArrowRight className="ml-2" />
        </button>
      </div>
    </div>
  );
}
