export default function Notification({ event }) {
  return (
    <div className="w-full h-auto relative">
      <div className="bg-[#6267f1] text-slate-100 mt-10 px-5 py-3.5 rounded-lg shadow hover:shadow-xl max-w-sm mx-auto">
        <div className="w-full flex items-center justify-between">
          <span className="font-medium text-sm">New Notification</span>
        </div>
        <div className="flex items-center mt-2 rounded-lg py-1 cursor-pointer">
          <div className="relative flex flex-shrink-0 items-end">
            <img className="h-16 w-16 rounded-full" src={event?.avatarUrl} />
          </div>
          <div className="ml-3.5">
            <span className="font-semibold tracking-tight text-sm">
              {event?.username}
            </span>
            <span className="text-xs leading-none opacity-50">
              {` added you as a participant for `}
            </span>
            <span className="font-semibold tracking-tight text-sm">
              {event?.title}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
