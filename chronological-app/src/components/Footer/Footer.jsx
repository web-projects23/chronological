import { SiGitlab } from "react-icons/si";
import { SiLinkedin } from "react-icons/si";

const navigation = [
  {
    name: "Gitlab",
    href: "https://gitlab.com/web-projects23/chronological",
    icon: () => <SiGitlab />,
  },
  {
    name: "LinkedIn",
    href: "#",
    icon: () => <SiLinkedin />,
  },
];

export default function Footer() {
  return (
    <footer className="bg-black">
      <div className="max-w-7xl mx-auto py-5 px-4 sm:px-6 md:flex md:items-center md:justify-between lg:px-8">
        <div className="flex justify-center space-x-6 md:order-2">
          {navigation.map((item) => (
            <a
              key={item.name}
              href={item.href}
              target="_blank"
              rel="noopener noreferrer"
              className="text-white"
            >
              <span className="sr-only">{item.name}</span>
              <item.icon className="h-6 w-6" aria-hidden="true" />
            </a>
          ))}
        </div>
        <div className="mt-8 md:mt-0 md:order-1">
          <p className="text-center text-base text-white">
            &copy; 2022 Think About Innovation - All rights reserved.
          </p>
        </div>
      </div>
    </footer>
  );
}
