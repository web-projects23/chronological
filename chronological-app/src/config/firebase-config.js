import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyARy5n7gyapKtAznMMzRpwKIMVc7MMmQy8",
  authDomain: "chronological2.firebaseapp.com",
  databaseURL: "https://chronological2-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "chronological2",
  storageBucket: "chronological2.appspot.com",
  messagingSenderId: "814886721468",
  appId: "1:814886721468:web:ae01de950811f2af16fdb9",
  measurementId: "G-W88XZEV1R5"
};

export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);
