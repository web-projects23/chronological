<img alt='ChronologiCal' src='./chronological-app/src/assets/images/Logo.png' /> 

# ChronologiCal

**[Website Home](https://chronological.netlify.app/)**

## Description
***Chronological*** is a single-page event calendar website that allows registered users to set events in their calendar for month, week, work week or for any day, add other users to participate in their event, create their own contact list, add users to their contact list, edit their contact list,edit the details of their own event, search for events or users, change their personal details, password or avatar or keep track of the weather at their current location.


## Visuals ( PC )
### Home ( Public part )
![Home view](./chronological-app/src/assets/images/home-view.png)
### Login
![Login page view](./chronological-app/src/assets/images/login.png)
### Register
![Register view](./chronological-app/src/assets/images/register.png)
### Calendar ( Month view )
![Calendar month view](./chronological-app/src/assets/images/calendar-month.png)
### Calendar ( Week view )
![Calendar week view](./chronological-app/src/assets/images/calendar-week.png)
### Calendar ( Work week view )
![Calendar work week view](./chronological-app/src/assets/images/calendar-work-week.png)
### Calendar ( Day view )
![Calendar day view](./chronological-app/src/assets/images/calendar-day.png)
### Create event 
![Calendar add event modal view](./chronological-app/src/assets/images/create-event.png)
### My events
![My events view](./chronological-app/src/assets/images/my-events.png)
### Create contact list
![Create contact list modal view](./chronological-app/src/assets/images/create-contacts.png)
### Contact list
![Contact list view](./chronological-app/src/assets/images/contacts.png)
### Edit contact list
![Edit contact list view](./chronological-app/src/assets/images/edit-contacts.png)
### Users
![Users view](./chronological-app/src/assets/images/users.png)
### Users ( Admin view )
![Users view](./chronological-app/src/assets/images/users-admin.png)
### About us
![About us view](./chronological-app/src/assets/images/about-us.png)
### View profile
![View other user profile view](./chronological-app/src/assets/images/view-profile.png)
### My profile
![My profile view](./chronological-app/src/assets/images/my-profile.png)
### Edit my profile ( Avatar )
![My profile view](./chronological-app/src/assets/images/edit-profile.png)
### Notifications
![Notifications view](./chronological-app/src/assets/images/notifications.png)

## Technologies
<img src="https://cdn-icons-png.flaticon.com/512/5968/5968292.png" width="18px">  JavaScript

<img src="https://icon-library.com/images/react-icon/react-icon-29.jpg" width="18px">  React 

<img src="https://vasterra.com/blog/wp-content/uploads/2021/08/Tailwind-img.png" width="18px">  TailwindCSS

<img src="https://www.gstatic.com/devrel-devsite/prod/v6bd99c7d8fa6220aea6020b29137b212e2eacdba13242535487f52ab3557b0cb/firebase/images/touchicon-180.png" width="18px">  Firebase 

## Authors 
Asen Georgiev [@Asen.G](Asen.G)

Ivana Georgieva [@Ivana1225](https://gitlab.com/Ivana1225)

Toni Velikov [@Skorpionas99](https://gitlab.com/Skorpionas99)
